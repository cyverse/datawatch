## Installation
An Ansible playbook is provided for installing DataWatch and launching it in kubernetes.

1. *Clone the DataWatch repository*: This typically goes into /opt/datawatch
2. *`cd /opt/datawatch/install`*
3. *`cp example-config.yaml config.yaml`*
4. *edit config.yaml*
    * make sure to set `DEPLOYMENT_TYPE` to `dev` or `prod`, which configures manifests to be deployed into `skaffold/dev` or `skaffold/install/k8s_manifests`. The manifests themselves will have different registry repo values.
    * make sure whichever directory is specified for `DATAWATCH_DEPLOY_DIR` is writeable by the current user
    * the two `PORT` variable values must be in double quotes so that they are interpreted as strings
5. *./deploy.sh*
6. Note, if `DEPLOYMENT_TYPE` == `dev`, developer must start datawatch by using skaffold