#!/bin/bash

# if [ $# -lt 1 ] || [[ "$1" != "local"  && "$1" != "prod" ]]; then
#     echo "Usage: $0 [local|prod]"
# 	  exit 1
# fi

echo "DEPLOY.SH: updating packages"
sudo apt-get -y update && sudo apt-get -y install ansible
if [ $? != 0 ]; then
    echo "DEPLOY.SH WARNING: something went wrong with installing ansible, but will try to ignore for now"
fi

# check if for a config.yaml
if [ ! -f config.yaml ]; then
    echo
    echo "DEPLOY.SH Missing 'config.yaml'. Copy example-config.yaml, edit, and rerun deploy.sh"
    echo
    exit 1
fi

# run requirements.yaml
echo "DEPLOY.SH installing ansible galaxy requirements as necessary"
ansible-galaxy role install -f -r requirements.yaml
if [ $? != 0 ]; then
    echo "DEPLOY.SH WARNING: ansible-galaxy failed, but will try to ignore for now"
fi

# run the playbook
ansible-playbook -i localhost.yaml playbook.yaml