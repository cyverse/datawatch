# DataWatch email service
This service receives events from the monitor service, bundles them together based on listener notify interval configuration and then emails the bundled events to the user.