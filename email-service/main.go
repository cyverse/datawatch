package main

import (
	"container/heap"
	"encoding/json"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/nats-io/stan.go"
	"gitlab.com/cyverse/datawatch/common"
)

// email - data to send to external email service
type email struct {
	File    string
	Body    string `json:"body"`
	From    string
	Subject string
	To      string
}

// message - data sent from monitor-service
type message struct {
	Details        string `json:"details"`
	ListenerID     string `json:"listenerID"`
	NotifyInterval int64  `json:"notifyInterval"`
	Message        string `json:"message"`
}

// notification - collection of events to send
type notification struct {
	Details        string
	Messages       []string
	ListenerID     string
	NotifyInterval int64
	Timestamp      int64
}

var notifications = make(map[string]*notification)
var pq = make(priorityQueue, 0)

var lock sync.RWMutex

func main() {
	heap.Init(&pq)

	emailConnection := common.StanConnection("email", "external_email-service")
	defer emailConnection.Close()
	monitorConnection := common.StanConnection("email", "monitor")
	defer monitorConnection.Close()

	sub, err := monitorConnection.Subscribe("datawatch.email", func(msg *stan.Msg) {
		slog.Info("received msg", "subject", msg.Subject, "pid", os.Getpid(), "msgData", string(msg.Data))
		var m message
		json.Unmarshal(msg.Data, &m)
		lock.Lock()
		v, ok := notifications[m.ListenerID]
		if ok {
			v.Messages = append(v.Messages, m.Message)
		} else {
			if m.NotifyInterval < common.EmailListenerType.MinNotifyInterval {
				m.NotifyInterval = common.EmailListenerType.MinNotifyInterval
			}
			n := notification{m.Details, []string{m.Message}, m.ListenerID, m.NotifyInterval, time.Now().Unix() + int64(m.NotifyInterval)}
			notifications[n.ListenerID] = &n
			pq.Push(&n)
		}
		lock.Unlock()
	})
	if err != nil {
		log.Fatalf("Problem subscribing to datawatch.email: %s", err)
	}
	defer sub.Unsubscribe()

	go notify(emailConnection)

	slog.Info("email-service waiting for messages, press ctrl+c to exit")
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)
	<-ch
	slog.Info("exiting email service")
}

func notify(c stan.Conn) {
	for {
		lock.Lock()
		now := time.Now().Unix()
		for pq.Len() > 0 && pq[0].Timestamp <= now {
			n := heap.Pop(&pq).(*notification)
			delete(notifications, n.ListenerID)
			details := struct {
				Email string `json:"email"`
			}{}
			json.Unmarshal([]byte(n.Details), &details)
			go sendEmail(details.Email, n.Messages, c)
		}
		lock.Unlock()
		time.Sleep(1 * time.Minute)
	}
}

func sendEmail(address string, messages []string, c stan.Conn) {
	var a []interface{}
	for _, m := range messages {
		var o map[string]interface{}
		json.Unmarshal([]byte(m), &o)
		a = append(a, o)
	}
	j, _ := json.MarshalIndent(a, "", "    ")
	slog.Info("email notification", "address", address, "json", string(j))
	j, _ = json.Marshal(email{From: "noreply@cyverse.org", To: address, Subject: "DataWatch Notification", Body: string(j), File: "simple-template.tmpl"})
	err := c.Publish("email", j)
	if err != nil {
		slog.Error("fail to publish msg", "error", err)
	}
}
