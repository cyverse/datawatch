package main

// DataSource - read only id + description for a data source
type DataSource struct {
	SourceID string `json:"sourceID"`

	Description string `json:"description,omitempty"`
}

var cyverse = DataSource{
	SourceID:    "cyverse",
	Description: "the CyVerse Data Store via iRODS",
}

// DataSources - array of known data sources
var DataSources = []*DataSource{
	&cyverse,
}

// GetDataSource - return structure with matching sourceID
func GetDataSource(sourceID string) *DataSource {
	for _, v := range DataSources {
		if v.SourceID == sourceID {
			return v
		}
	}
	return nil
}
