package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

type route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// newRouter - create and return a mux.Router
func newRouter() *mux.Router {
	Router := mux.NewRouter().StrictSlash(true)
	Router.Use(LoggerMiddleware)
	for _, Route := range routes {
		var handler = Route.HandlerFunc

		Router.
			Methods(Route.Method).
			Path(Route.Pattern).
			Name(Route.Name).
			Handler(handler)
	}

	return Router
}

var routes = []route{
	{
		"AccessTokenGet",
		"GET",
		"/accessToken",
		AccessTokenGet,
	},
	{
		"DataSourcesGet",
		"GET",
		"/dataSources",
		DataSourcesGet,
	},
	{
		"DataSourcesSourceIDGet",
		"GET",
		"/dataSources/{sourceID}",
		DataSourcesSourceIDGet,
	},
	{
		"DbGet",
		"GET",
		"/db",
		DbGet,
	},
	{
		"KeycloakTokenGet",
		"GET",
		"/keycloakToken",
		KeycloakTokenGet,
	},
	{
		"ListenersGet",
		"GET",
		"/listeners",
		ListenersGet,
	},
	{
		"ListenersListenerIDDelete",
		"DELETE",
		"/listeners/{listenerID}",
		ListenersListenerIDDelete,
	},
	{
		"ListenersListenerIDGet",
		"GET",
		"/listeners/{listenerID}",
		ListenersListenerIDGet,
	},
	{
		"ListenersListenerIDPut",
		"PUT",
		"/listeners/{listenerID}",
		ListenersListenerIDPut,
	},
	{
		"ListenersListenerIDTrigger",
		"POST",
		"/listeners/{listenerID}",
		ListenersListenerIDTrigger,
	},
	{
		"ListenersPost",
		"POST",
		"/listeners",
		ListenersPost,
	},
	{
		"ListenerTypesGet",
		"GET",
		"/listenerTypes",
		ListenerTypesGet,
	},
	{
		"State",
		"GET",
		"/state",
		StateGet,
	},
	{
		"UsersGet",
		"GET",
		"/users",
		UsersGet,
	},
	{
		"UsersPost",
		"POST",
		"/users",
		UsersPost,
	},
	{
		"UsersUsernameDelete",
		"DELETE",
		"/users/{username}",
		UsersUsernameDelete,
	},
	{
		"UsersUsernameGet",
		"GET",
		"/users/{username}",
		UsersUsernameGet,
	},
	{
		"UsersUsernamePut",
		"PUT",
		"/users/{username}",
		UsersUsernamePut,
	},
	{
		"HomePage",
		"GET",
		"/",
		HomePage,
	},
}
