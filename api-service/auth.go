package main

import (
	"log/slog"
	"net/http"
)

// AuthenticationMiddleware is a middleware function required on some routes to
// make sure a user is authenticated and authorizaed to access that endpoint
func AuthenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		if r.URL.Path == "/" || r.URL.Path == "/keycloakToken" {
			next.ServeHTTP(w, r)
			return
		}
		err := authorize(w, r)
		if err != nil {
			slog.Error("authorize error", "error", err)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// authorize is a helper to read the token header and verify it against Keycloak.
// It will also get the username from the token
func authorize(w http.ResponseWriter, r *http.Request) error {
	accessToken := r.Header.Get("Authorization")
	idToken, err := keycloakClient.Verify(accessToken)
	if err != nil {
		slog.Error("keycloak verify", "error", err)
		sendError(err, http.StatusForbidden, w, r)
		return err
	}
	idTokenClaims := struct {
		Username string `json:"preferred_username"`
		Email    string `json:"email"`
		Admin    string `json:"potami_admin"`
	}{}
	err = idToken.Claims(&idTokenClaims)
	if err != nil {
		slog.Error("token claim", "error", err)
		sendError(err, http.StatusInternalServerError, w, r)
		return err
	}
	r.Header.Set("X-DataWatch-User", idTokenClaims.Username)
	r.Header.Set("X-DataWatch-Email", idTokenClaims.Email)
	if idTokenClaims.Admin == "[potami_admin]" {
		r.Header.Set("X-DataWatch-Admin", "true")
	} else {
		r.Header.Set("X-DataWatch-Admin", "false")
	}
	return nil
}
