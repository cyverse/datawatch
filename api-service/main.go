package main

import (
	"log"
	"log/slog"
	"net/http"
	"os"

	"github.com/nats-io/nats.go"
	"gitlab.com/cyverse/datawatch/common"
)

var monitorConnection *nats.Conn

func main() {
	method := os.Getenv("AUTHENTICATION_METHOD")
	slog.Info("api-service", "authentication method", method)
	common.InitDB()
	monitorConnection = common.NatsConnection()
	defer monitorConnection.Close()
	router := newRouter()
	if method == "keycloak" {
		InitKeycloakClient()
		router.Use(AuthenticationMiddleware)
	}
	log.Fatal(http.ListenAndServe(":80", router))
}
