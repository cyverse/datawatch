package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/google/uuid"
	"gitlab.com/cyverse/datawatch/common"
)

type listener common.Listener

// DeleteListener - delete listener with listenerID belonging to user with username
func deleteListener(listenerID string, username string) error {
	cl, err := common.LoadListener(listenerID, username)
	if err != nil {
		return errors.New("listener with ID " + listenerID + "not found")
	}
	if cl == nil {
		return errors.New("listener not found")
	}
	return common.Execute("MATCH (:User {username:$username})-[LISTENS_TO]->(l:Listener {listenerID:$listenerID}) OPTIONAL MATCH (l)-[:HAS_PATH]->(p) DETACH DELETE l,p", map[string]interface{}{"listenerID": listenerID, "username": username})
}

// send - write listener as JSON to response
func (l *listener) send(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(l.toJSON()))
}

// set - populate a Listener from Request query string
func (l *listener) set(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		l.ListenerID = uuid.New().String()
	}
	details := make(map[string]interface{})
	json.Unmarshal([]byte(l.Details), &details)
	r.ParseForm()
	for k, v := range r.Form {
		switch k {
		case "enabled":
			b, err := strconv.ParseBool(v[0])
			if err == nil {
				l.Enabled = b
			}
		case "eventFilters":
			l.EventFilters = v[0]
		case "header":
			details["headers"] = v
		case "listenerTypeID":
			l.ListenerTypeID = v[0]
		case "name":
			l.Name = v[0]
		case "notifyInterval":
			i, err := strconv.Atoi(v[0])
			if err == nil {
				l.NotifyInterval = int64(i)
			}
		case "param":
			details["params"] = v
		case "sourceID":
			l.SourceID = v[0]
		case "path":
			l.Paths = v
		default:
			details[k] = v[0]
		}
	}
	if l.ListenerTypeID == "" {
		sendError(errors.New("parameter listenerTypeID not specified"), http.StatusBadRequest, w, r)
		return
	}
	t := common.GetListenerType(l.ListenerTypeID)
	if t == nil {
		sendError(errors.New("unknown ListenerTypeID: "+l.ListenerTypeID), http.StatusBadRequest, w, r)
		return
	}
	if l.SourceID == "" {
		sendError(errors.New("parameter sourceID not specified"), http.StatusBadRequest, w, r)
		return
	}
	s := GetDataSource(l.SourceID)
	if s == nil {
		sendError(errors.New("unknown SourceID: "+l.SourceID), http.StatusBadRequest, w, r)
		return
	}
	if err := t.Validate(details); err != nil {
		sendError(err, http.StatusBadRequest, w, r)
		return
	}
	j, _ := json.Marshal(details)
	l.Details = string(j)
}

// store - store Listener in db, linked to User with username
func (l *listener) store() error {
	m := map[string]interface{}{"username": l.Username, "listenerID": l.ListenerID, "listenerTypeID": l.ListenerTypeID, "sourceID": l.SourceID, "details": l.Details, "enabled": l.Enabled}
	if l.EventFilters != "" {
		m["eventFilters"] = l.EventFilters
	}
	if l.Name != "" {
		m["name"] = l.Name
	}
	if l.NotifyInterval != 0 {
		m["notifyInterval"] = l.NotifyInterval
	}
	for i, v := range l.Paths {
		m["path"+strconv.Itoa(i)] = v
	}
	cypher := "MATCH (u:User {username:$username}) MERGE (u)-[:LISTENS_TO]->(l:Listener {listenerID:$listenerID}) SET l.listenerTypeID=$listenerTypeID,l.sourceID=$sourceID,l.details=$details,l.enabled=$enabled"
	if l.EventFilters != "" {
		cypher += ",l.eventFilters=$eventFilters"
	}
	if l.Name != "" {
		cypher += ",l.name=$name"
	}
	if l.NotifyInterval != 0 {
		cypher += ",l.notifyInterval=$notifyInterval"
	}
	cypher += " WITH l MATCH (l)-[:HAS_PATH]->(p) DETACH DELETE p"
	err := common.Execute(cypher, m)
	if err != nil {
		return err
	}
	cypher = "MATCH (u:User {username:$username})-[:LISTENS_TO]->(l:Listener {listenerID:$listenerID}) CREATE "
	for i := range l.Paths {
		if i > 0 {
			cypher += ","
		}
		cypher += "(l)-[:HAS_PATH]->({path:$path" + strconv.Itoa(i) + "})"
	}
	err = common.Execute(cypher, m)
	if err != nil {
		return err
	}
	return nil
}

// toJSON - return json string of listener + flattened details
func (l *listener) toJSON() string {
	j, _ := json.Marshal(l)
	var m map[string]interface{}
	json.Unmarshal(j, &m)
	var m2 map[string]interface{}
	json.Unmarshal([]byte(l.Details), &m2)
	for k := range m2 {
		m[k] = m2[k]
	}
	j, _ = json.Marshal(m)
	return string(j)
}
