package main

import (
	"encoding/json"
	"errors"
	"log/slog"
	"net/http"
	"strings"
	"time"

	"gitlab.com/cyverse/datawatch/common"
)

// apiError - structure for returning the description of an API error
type apiError struct {
	Timestamp time.Time `json:"timestamp"`
	Status    int       `json:"status"`
	Error     string    `json:"error"`
	Path      string    `json:"path"`
	Method    string    `json:"method"`
}

// getPathSegment - return the ith path segment
func getPathSegment(i int, name string, w http.ResponseWriter, r *http.Request) string {
	segments := strings.Split(r.URL.Path, "/")
	if i < len(segments) {
		return segments[i]
	}
	sendError(errors.New(name+" missing from path"), http.StatusBadRequest, w, r)
	return ""
}

// getUser - load user from request
func getUser(w http.ResponseWriter, r *http.Request) *common.User {
	username := r.Header.Get("X-DataWatch-User")
	if username == "" {
		sendError(errors.New("username not specified"), http.StatusUnauthorized, w, r)
		return nil
	}
	u, err := common.LoadUser(username)
	if err != nil {
		slog.Error("fail to load user", "error", err)
		sendError(errors.New("error loading user"), http.StatusBadRequest, w, r)
		return nil
	}
	if u.Username == "" {
		// automatically create user
		u.Username = username
		u.Email = r.Header.Get("X-DataWatch-Email")
		u.Admin = r.Header.Get("X-DataWatch-Admin") == "true"
		(*user)(u).store()
		// sendError(errors.New("User "+username+" not found"), http.StatusExpectationFailed, w, r)
		// return nil
	}
	return u
}

// sendData - return requested data to user
func sendData(data interface{}, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	j, _ := json.Marshal(data)
	w.Write(j)
}

// sendError - return error to user
func sendError(err error, errCode int, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(errCode)
	j, _ := json.Marshal(apiError{time.Now(), errCode, err.Error(), r.URL.Path, r.Method})
	w.Write(j)
}
