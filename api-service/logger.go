package main

import (
	"log/slog"
	"net/http"
	"time"
)

// LoggerMiddleware - return a handler that just logs requests
func LoggerMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		respW := passThroughRespWriter{w: w}
		start := time.Now()

		handler.ServeHTTP(&respW, r)

		slog.Info("",
			"method", r.Method,
			"uri", r.RequestURI,
			"status", respW.statusCode,
			"resp_bytes", respW.byteCount,
			"t_ms", time.Since(start).Milliseconds(),
			"username", r.Header.Get("X-DataWatch-User"),
		)
	})
}

type passThroughRespWriter struct {
	w          http.ResponseWriter
	statusCode int
	byteCount  uint64
}

func (p *passThroughRespWriter) Header() http.Header {
	return p.w.Header()
}

func (p *passThroughRespWriter) Write(bytes []byte) (int, error) {
	p.byteCount += uint64(len(bytes))
	return p.w.Write(bytes)
}

func (p *passThroughRespWriter) WriteHeader(statusCode int) {
	p.statusCode = statusCode
	p.w.WriteHeader(statusCode)
}
