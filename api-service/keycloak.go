package main

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"

	"github.com/coreos/go-oidc"
	"golang.org/x/oauth2"
)

var (
	keycloakClient *Client
)

// InitKeycloakClient is used to define Keycloak connection variables
func InitKeycloakClient() (err error) {
	keycloakClient, err = NewClient(
		os.Getenv("KEYCLOAK_URL"),
		os.Getenv("KEYCLOAK_REDIRECT_URL"),
		os.Getenv("KEYCLOAK_ID"),
		os.Getenv("KEYCLOAK_SECRET"),
	)
	return
}

// Client contains necessary structs for OpenID Connect with Keycloak
type Client struct {
	Provider     *oidc.Provider
	Verifier     *oidc.IDTokenVerifier
	OAuthConfig  oauth2.Config
	OpenIDConfig *oidc.Config
	URL          string
}

// GetToken fetches an access token from keycloak using a username and password
func GetToken(username, password string) (string, error) {
	resp, err := http.PostForm(os.Getenv("KEYCLOAK_URL")+"/protocol/openid-connect/token",
		url.Values{"scope": {"openid"}, "client_id": {os.Getenv("KEYCLOAK_ID")}, "client_secret": {os.Getenv("KEYCLOAK_SECRET")}, "grant_type": {"password"}, "username": {username}, "password": {password}})
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	var m map[string]interface{}
	json.Unmarshal(body, &m)
	token, ok := m["access_token"]
	if !ok {
		return "", errors.New("access_token not found -- username and/or password may be incorrect")
	}
	return token.(string), nil
}

// NewClient will create a client for interacting with a Keycloak server
func NewClient(url, redirect, id, secret string) (client *Client, err error) {
	ctx := context.Background()
	client = &Client{URL: url}
	provider, err := oidc.NewProvider(ctx, url)
	if err != nil {
		log.Fatal(err)
		return
	}
	client.Provider = provider
	client.OAuthConfig = oauth2.Config{
		ClientID:     id,
		ClientSecret: secret,
		RedirectURL:  redirect,
		Endpoint:     provider.Endpoint(),
		Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
	}
	client.OpenIDConfig = &oidc.Config{ClientID: id}
	client.Verifier = client.Provider.Verifier(client.OpenIDConfig)
	return
}

// Verify contacts the OpenID Connect provider to verify a token
func (client *Client) Verify(token string) (*oidc.IDToken, error) {
	return client.Verifier.Verify(context.Background(), token)
}
