package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"gitlab.com/cyverse/datawatch/common"

	"github.com/dchest/uniuri"
	"github.com/neo4j/neo4j-go-driver/neo4j"
)

// AccessTokenGet - generate return a new personal access token
func AccessTokenGet(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	token := uniuri.New()
	err := (*user)(u).setAccessToken(token)
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	w.Write([]byte("\"" + token + "\""))
}

// DataSourcesGet - Gets an array of data sources for listening to events
func DataSourcesGet(w http.ResponseWriter, r *http.Request) {
	sendData(DataSources, w)
}

// DataSourcesSourceIDGet - Gets a source by id
func DataSourcesSourceIDGet(w http.ResponseWriter, r *http.Request) {
	sourceID := getPathSegment(2, "sourceID", w, r)
	if sourceID == "" {
		return
	}
	for _, dataSource := range DataSources {
		if dataSource.SourceID == sourceID {
			sendData(dataSource, w)
			return
		}
	}
	sendError(errors.New("DataSource with id "+sourceID+" not found"), http.StatusNotFound, w, r)
}

// DbGet - dump db contents, must be admin
func DbGet(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil || !u.Admin {
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	fmt.Fprint(w, "\"")
	common.Load("MATCH (u:User) RETURN u.username,u.email,u.admin", nil, func(record neo4j.Record) {
		var u common.User
		u.Load(record)
		fmt.Fprintf(w, "./dw create user %s %s", u.Username, u.Email)
		if u.Admin {
			fmt.Fprint(w, " -admin")
		}
		fmt.Fprint(w, "\\n")
		listeners, _ := common.GetListeners(u.Username)
		for _, l := range listeners {
			fmt.Fprintf(w, "./dw create listener -username %s -t %s -s %s", u.Username, l.ListenerTypeID, l.SourceID)
			if !l.Enabled {
				fmt.Fprint(w, " -enabled=false")
			}
			if len(l.EventFilters) > 0 {
				fmt.Fprintf(w, " -f %s", l.EventFilters)
			}
			if len(l.Name) > 0 {
				fmt.Fprintf(w, " -n %s", l.Name)
			}
			if l.NotifyInterval != 0 {
				fmt.Fprintf(w, " -i %d", l.NotifyInterval)
			}
			var m map[string]interface{}
			json.Unmarshal([]byte(l.Details), &m)
			switch l.ListenerTypeID {
			case "email":
				details := struct {
					Email string `json:"email"`
				}{}
				json.Unmarshal([]byte(l.Details), &details)
				if details.Email != "" {
					fmt.Fprintf(w, " -e %s", details.Email)
				}
			case "webdav":
				details := struct {
					Authentication string   `json:"authentication"`
					Headers        []string `json:"headers"`
					URL            string   `json:"url"`
				}{}
				json.Unmarshal([]byte(l.Details), &details)
				if details.Authentication != "" {
					fmt.Fprintf(w, " -a %s", details.Authentication)
				}
				if details.URL != "" {
					fmt.Fprintf(w, " -u %s", details.URL)
				}
				for _, h := range details.Headers {
					fmt.Fprintf(w, " -h %s", h)
				}
			case "webhook":
				details := struct {
					Authentication string   `json:"authentication"`
					Headers        []string `json:"headers"`
					Method         string   `json:"method"`
					Params         []string `json:"params"`
					URL            string   `json:"url"`
				}{}
				json.Unmarshal([]byte(l.Details), &details)
				if details.Authentication != "" {
					fmt.Fprintf(w, " -a %s", details.Authentication)
				}
				if details.Method != "" {
					fmt.Fprintf(w, " -m %s", details.Method)
				}
				if details.URL != "" {
					fmt.Fprintf(w, " -u %s", details.URL)
				}
				for _, h := range details.Headers {
					fmt.Fprintf(w, " -h %s", h)
				}
				for _, p := range details.Params {
					fmt.Fprintf(w, " -p %s", p)
				}
			}
			for _, p := range l.Paths {
				fmt.Fprintf(w, " %s", p)
			}
			fmt.Fprint(w, "\\n")
		}
	})
	fmt.Fprint(w, "\"")
}

// HomePage - web GUI for API
func HomePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "<html><body><h1>DataWatch</h1><p>A web interface for accessing the API is being developed. In the meantime, please use the API directly or the CLI.</p><p>More info can be found at <a href=\"https://gitlab.com/cyverse/datawatch\">https://gitlab.com/cyverse/datawatch</a></p></body></html>")
}

// KeycloakTokenGet - return an access token from keycloak
func KeycloakTokenGet(w http.ResponseWriter, r *http.Request) {
	token, err := GetToken(r.FormValue("username"), r.FormValue("password"))
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("\"" + token + "\""))
}

// ListenersGet - Gets a list of listeners for the current user
func ListenersGet(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	username := u.Username
	if u.Admin && r.FormValue("username") != "" {
		username = r.FormValue("username")
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("["))
	listeners, _ := common.GetListeners(username)
	for i, cl := range listeners {
		if i > 0 {
			w.Write([]byte(","))
		}
		var l *listener = (*listener)(cl)
		w.Write([]byte(l.toJSON()))
	}
	w.Write([]byte("]"))
}

// ListenersListenerIDDelete - delete a listener
func ListenersListenerIDDelete(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	listenerID := getPathSegment(2, "listenerID", w, r)
	if listenerID == "" {
		return
	}
	err := deleteListener(listenerID, u.Username)
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// ListenersListenerIDGet - Gets information about a listener by id
func ListenersListenerIDGet(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	listenerID := getPathSegment(2, "listenerID", w, r)
	if listenerID == "" {
		return
	}
	cl, err := common.LoadListener(listenerID, u.Username)
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	if cl == nil {
		sendError(errors.New("listener not found"), http.StatusInternalServerError, w, r)
		return
	}
	var l *listener = (*listener)(cl)
	l.send(w)
}

// ListenersListenerIDPut - update a listener
func ListenersListenerIDPut(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	listenerID := getPathSegment(2, "listenerID", w, r)
	if listenerID == "" {
		sendError(errors.New("listener ID not specified"), http.StatusInternalServerError, w, r)
		return
	}
	cl, err := common.LoadListener(listenerID, u.Username)
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	if cl == nil {
		sendError(errors.New("listener not found"), http.StatusInternalServerError, w, r)
		return
	}
	var l *listener = (*listener)(cl)
	l.set(w, r)
	err = l.store()
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	l.send(w)
}

// ListenersListenerIDTrigger - tell DataWatch to publish a test notification for the specified listener
func ListenersListenerIDTrigger(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	listenerID := getPathSegment(2, "listenerID", w, r)
	if listenerID == "" {
		sendError(errors.New("listener ID not specified"), http.StatusInternalServerError, w, r)
		return
	}
	data := r.FormValue("data")
	if data == "" {
		sendError(errors.New("data parameter not specified"), http.StatusInternalServerError, w, r)
		return
	}
	json, _ := json.Marshal(map[string]interface{}{"trigger": listenerID, "username": u.Username, "data": data})
	slog.Info("triggering listener", "json", string(json), "username", u.Username, "listenerID", listenerID)
	monitorConnection.Publish("datawatch.api", json)
}

// ListenersPost - Creates a new listener
func ListenersPost(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	if r.FormValue("enabled") == "" {
		r.Form.Add("enabled", "true")
	}
	var l listener
	l.Username = u.Username
	if r.FormValue("username") != "" {
		if u.Admin {
			l.Username = r.FormValue("username")
			_, err := common.LoadUser(l.Username)
			if err != nil {
				sendError(errors.New("user "+l.Username+" does not exist"), http.StatusBadRequest, w, r)
				return
			}
		} else {
			sendError(errors.New("username parameter set by non-admin user"), http.StatusBadRequest, w, r)
			return
		}
	}
	l.set(w, r)
	if len(l.Paths) == 0 {
		sendError(errors.New("required parameter path not specified"), http.StatusBadRequest, w, r)
		return
	}
	err := l.store()
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	l.send(w)
}

// ListenerTypesGet - Gets an array of listener types
func ListenerTypesGet(w http.ResponseWriter, r *http.Request) {
	sendData(common.ListenerTypes, w)
}

// StateGet -
func StateGet(w http.ResponseWriter, r *http.Request) {
	if admin := r.Header.Get("X-DataWatch-Admin") == "true"; !admin {
		return
	}
	msg, err := monitorConnection.Request("datawatch.api", []byte("state"), 2*time.Second)
	if err == nil {
		sendData(string(msg.Data), w)
	}
}

// UsersGet - Gets an array of users. Non-administrative users will only get one user, self.
func UsersGet(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	if u.Admin {
		var users []*common.User
		common.Load("MATCH (u:User) RETURN u.username,u.email,u.admin", nil, func(record neo4j.Record) {
			var user common.User
			user.Load(record)
			users = append(users, &user)
		})
		sendData(users, w)
	} else {
		sendData(u, w)
	}
}

// UsersPost - Create a new user, currently overwrites existing user
func UsersPost(w http.ResponseWriter, r *http.Request) {
	admin := r.Header.Get("X-DataWatch-Admin") == "true"
	if !admin {
		u := getUser(w, r)
		if u == nil {
			return
		}
		if !u.Admin {
			sendError(errors.New("unauthorized access"), http.StatusUnauthorized, w, r)
			return
		}
	}
	var user user
	user.set(r)
	if user.Username == "" {
		sendError(errors.New("required parameter username not specified"), http.StatusBadRequest, w, r)
		return
	}
	if user.Email == "" {
		sendError(errors.New("required parameter email not specified"), http.StatusBadRequest, w, r)
		return
	}
	u, err := common.LoadUser(user.Username)
	if err == nil && u.Username != "" {
		sendError(errors.New("user "+user.Username+" already exists"), http.StatusBadRequest, w, r)
		return
	}
	err = user.store()
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	sendData(user, w)
}

// UsersUsernameDelete - Delete an existing user; only for admins
func UsersUsernameDelete(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	if !u.Admin {
		sendError(errors.New("unauthorized access"), http.StatusUnauthorized, w, r)
		return
	}
	username := getPathSegment(2, "username", w, r)
	if username == "" {
		return
	}
	err := deleteUser(username)
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// UsersUsernameGet - Get existing user information; admins can get any user, everyone else can only get their own user
func UsersUsernameGet(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	if !u.Admin {
		sendData(u, w)
		return
	}
	username := getPathSegment(2, "username", w, r)
	if username == "" {
		return
	}
	u, err := common.LoadUser(username)
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	if u.Username == "" {
		sendError(errors.New("user "+username+" not found"), http.StatusInternalServerError, w, r)
		return
	}
	sendData(u, w)
}

// UsersUsernamePut - Update existing user; admins can modify any user, everyone else can only change their own user
// doesn't currently allow for username change
func UsersUsernamePut(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	if u == nil {
		return
	}
	username := getPathSegment(2, "username", w, r)
	if username == "" {
		return
	}
	admin := u.Admin || r.Header.Get("X-DataWatch-Admin") == "true"
	if username != u.Username && !admin {
		sendError(errors.New("unauthorized access"), http.StatusUnauthorized, w, r)
		return
	}
	u, err := common.LoadUser(username)
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	if u.Username == "" {
		sendError(errors.New("user "+username+" not found"), http.StatusInternalServerError, w, r)
		return
	}
	(*user)(u).set(r)
	err = (*user)(u).store()
	if err != nil {
		sendError(err, http.StatusInternalServerError, w, r)
		return
	}
	sendData(u, w)
}
