package main

import (
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/cyverse/datawatch/common"
)

type user common.User

// deleteUser - delete user with username
func deleteUser(username string) error {
	_, err := common.LoadUser(username)
	if err != nil {
		return errors.New("user with username " + username + " not found")
	}
	return common.Execute("MATCH (u:User {username:$username}) OPTIONAL MATCH (u)-[LISTENS_TO]->(l:Listener) DETACH DELETE u,l", map[string]interface{}{"username": username})
}

// set - popuplate User from Request query string
func (u *user) set(r *http.Request) {
	if v := r.FormValue("username"); v != "" {
		u.Username = v
	}
	if v := r.FormValue("email"); v != "" {
		u.Email = v
	}
	if v := r.FormValue("admin"); v != "" {
		admin, err := strconv.ParseBool(v)
		if err == nil {
			u.Admin = admin
		}
	}
}

// setAccessToken - store the personal access token for a user in the db
func (u *user) setAccessToken(accessToken string) error {
	return common.Execute("MERGE (u:User {username:$username}) SET u.accessToken=$accessToken", map[string]interface{}{"username": u.Username, "accessToken": accessToken})
}

// store - store user in the db
func (u *user) store() error {
	if u.Admin {
		return common.Execute("MERGE (u:User {username:$username}) SET u.email=$email,u.admin=true", map[string]interface{}{"username": u.Username, "email": u.Email})
	}
	return common.Execute("MERGE (u:User {username:$username}) SET u.email=$email", map[string]interface{}{"username": u.Username, "email": u.Email})
}
