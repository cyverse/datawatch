# DataWatch API Service
this service provides a RESTful API for configuring users and listeners. Open API docs can be found [here](https://gitlab.com/cyverse/datawatch/-/blob/master/docs/openapi/datawatch-openapi.yaml).

## Examples
all examples will use datawatch.cyverse.org as the domain for the API and `curl` for demonstrating calls to the API.

### Authentication
Before calling the API you need to obtain a keycloak token. It is recommended to surround the URL with single quotes so that the shell doesn't interpret any characters.
```
curl 'https://datawatch.cyverse.org/keycloakToken?username=<username>&password=<password>'
```
a keycloak token will be returned in double quotes. Do not include the double quotes when using the token.

### Get Your User Info
```
curl -H "Authorization: <token>" https://datawatch.cyverse.org/users
```
User info is currently the username, the user's email address, and optionally the "admin" flag
> Note: admin users will get a list of all the users

### Get Listeners
```
curl -H "Authorization: <token>" https://datawatch.cyverse.org/listeners
```

### Add Listener
```
curl -H "Authorization: <token>" https://datawatch.cyverse.org/listeners -d 'listenerTypeID=email&sourceID=cyverse&path=/iplant/home/<username>/test'
```
listenerTypeID can be "email", "webdav", or "webhook". The optional parameter `notifyInterval` may be used to override the default amount of time events are bundled together before being sent as a single notification. For example: `&notifyInterval=600` will bundle events for this listener for 10 minutes.