# DataWatch Path Fetcher 

This service fetches the path for a iRODS data object specified by a UUID.<br>

<!---
### To build the service, run the following command in the terminal window:

`docker build -t <tag name> .`

#### Run the service:

`docker run -e "iuser=<irods username>" -e "ipw=<irods password>" -e "ihost=<irods host>" -e "iport=<irods port>" -e "izone=<irods zone>" <tag name>`

-->
