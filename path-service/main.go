package main

import (
	"encoding/json"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"strconv"

	irodsfs_client "github.com/cyverse/go-irodsclient/fs"
	irodsfs_clienttype "github.com/cyverse/go-irodsclient/irods/types"
	"github.com/nats-io/nats.go"
	"gitlab.com/cyverse/datawatch/common"
)

var account = irodsfs_clienttype.IRODSAccount{
	AuthenticationScheme:    irodsfs_clienttype.AuthSchemeNative,
	ClientServerNegotiation: false,
	CSNegotiationPolicy:     irodsfs_clienttype.CSNegotiationRequireTCP,
	Host:                    os.Getenv("ihost"),
	Port:                    atoi(os.Getenv("iport")),
	ClientUser:              os.Getenv("iuser"),
	ClientZone:              os.Getenv("izone"),
	ProxyUser:               os.Getenv("iuser"),
	ProxyZone:               os.Getenv("izone"),
	Password:                os.Getenv("ipw"),
	PamTTL:                  irodsfs_clienttype.PamTTLDefault,
	SSLConfiguration:        nil,
}

var fsclient *irodsfs_client.FileSystem

// job - struct holding id and path
type job struct {
	id   string
	path chan string
}

var jobs chan job

func main() {
	var err error
	fsclient, err = irodsfs_client.NewFileSystemWithDefault(&account, "datawatch-path-service")
	if err != nil {
		log.Fatalf("%s: %s", "Error connecting to iRODS", err)
	}
	defer fsclient.Release()

	monitorConnection := common.NatsConnection()
	defer monitorConnection.Close()

	const numWorkers = 10
	jobs = make(chan job, numWorkers)
	for i := 0; i < numWorkers; i++ {
		go worker(jobs)
	}

	sub, err := monitorConnection.Subscribe("datawatch.paths", func(msg *nats.Msg) {
		// slog.Info("path-service received msg", "subject", msg.Subject, "queue", msg.Sub.Queue, "pid", os.Getpid(), "msgData", string(msg.Data))
		job := makeJob(msg)
		jobs <- job
		path := <-job.path
		json, _ := json.Marshal(path)
		// slog.Info("Sending msg", "json", string(json))
		msg.Respond(json)
	})
	if err != nil {
		log.Fatalf("Problem subscribing to datawatch.paths: %s", err)
	}
	defer sub.Unsubscribe()

	slog.Info("path-service listening on subject datawatch.paths, press ctrl+c to exit")
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	slog.Info("exiting path service")
}

func atoi(port string) int {
	i, err := strconv.Atoi(port)
	if err != nil {
		log.Fatal(err)
	}
	return i
}

func fetchPath(ipcUUID string) string {
	entries, err := fsclient.SearchByMeta("ipc_UUID", ipcUUID)
	if err == nil {
		// only one entry must be found
		if len(entries) == 1 {
			// return full path of the data object or the collection
			return entries[0].Path
		}
	}

	// if we couldn't find, return empty string
	return ""
}

func makeJob(msg *nats.Msg) job {
	var job job
	var o map[string]interface{}
	if err := json.Unmarshal(msg.Data, &o); err != nil {
		slog.Error("json unmarshal error", "error", err)
	}
	job.id = o["id"].(string)
	job.path = make(chan string)
	return job
}

func worker(jobs <-chan job) {
	for job := range jobs {
		job.path <- fetchPath(job.id)
	}
}
