package main

// priorityQueue - a priority queue to order notification by timestamp
type priorityQueue []*notification

func (pq priorityQueue) Len() int { return len(pq) }

func (pq priorityQueue) Less(i, j int) bool {
	return pq[i].Timestamp < pq[j].Timestamp
}

// Pop - remove and return the soonest Events
func (pq *priorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil // avoid memory leak
	*pq = old[0 : n-1]
	return item
}

// Push - add an notification to the queue
func (pq *priorityQueue) Push(x interface{}) {
	item := x.(*notification)
	*pq = append(*pq, item)
}

func (pq priorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}
