package main

import (
	"bytes"
	"container/heap"
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"

	"github.com/nats-io/stan.go"
	"gitlab.com/cyverse/datawatch/common"
)

// message - data sent from monitor-service
type message struct {
	Details        string `json:"details"`
	ListenerID     string `json:"listenerID"`
	NotifyInterval int64  `json:"notifyInterval"`
	Message        string `json:"message"`
}

// notification - collection of events to send
type notification struct {
	Details        string
	Messages       []string
	ListenerID     string
	NotifyInterval int64
	Timestamp      int64
}

var notifications = make(map[string]*notification)
var pq = make(priorityQueue, 0)

var lock sync.RWMutex

func main() {
	heap.Init(&pq)

	monitorConnection := common.StanConnection("webdav", "monitor")
	defer monitorConnection.Close()

	sub, err := monitorConnection.Subscribe("datawatch.webdav", func(msg *stan.Msg) {
		// slog.Info("Received msg", "subject", msg.Subject, "pid", os.Getpid(), "msgData", string(msg.Data))
		var m message
		json.Unmarshal(msg.Data, &m)
		lock.Lock()
		v, ok := notifications[m.ListenerID]
		if ok {
			v.Messages = append(v.Messages, m.Message)
		} else {
			if m.NotifyInterval < common.WebDAVListenerType.MinNotifyInterval {
				m.NotifyInterval = common.WebDAVListenerType.MinNotifyInterval
			}
			n := notification{m.Details, []string{m.Message}, m.ListenerID, m.NotifyInterval, time.Now().Unix() + int64(m.NotifyInterval)}
			notifications[n.ListenerID] = &n
			pq.Push(&n)
		}
		lock.Unlock()
	})
	if err != nil {
		log.Fatalf("Problem subscribing to datawatch.webdav: %s", err)
	}
	defer sub.Unsubscribe()

	go notify()

	slog.Info("webdav-service waiting for messages, press ctrl+c to exit")
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	slog.Info("exiting webdav service")
}

func notify() {
	for {
		lock.Lock()
		now := time.Now().Unix()
		for pq.Len() > 0 && pq[0].Timestamp <= now {
			n := heap.Pop(&pq).(*notification)
			delete(notifications, n.ListenerID)
			details := struct {
				Authentication string   `json:"authentication"`
				Headers        []string `json:"headers"`
				URL            string   `json:"url"`
			}{}
			json.Unmarshal([]byte(n.Details), &details)
			go webdav(details.URL, details.Authentication, details.Headers, n.Messages)
		}
		lock.Unlock()
		time.Sleep(1 * time.Minute)
	}
}

func webdav(url string, authentication string, headers []string, messages []string) {
	if !strings.Contains(url, "://") {
		url = "http://" + url
	}
	if !strings.HasSuffix(url, "/") {
		url += "/"
	}
	url += "DataWatch" + time.Now().UTC().Format("_2006_01_02_15_04_05.9999999_MST") + ".json"

	var a []interface{}
	for _, m := range messages {
		var o map[string]interface{}
		json.Unmarshal([]byte(m), &o)
		a = append(a, o)
	}
	j, _ := json.MarshalIndent(a, "", "    ")
	client := &http.Client{}
	var req *http.Request
	var err error
	slog.Info("webhook notification", "url", url, "method", "PUT", "body", string(j))
	req, err = http.NewRequest("PUT", url, bytes.NewReader(j))
	if err != nil {
		slog.Error("fail to create request", "error", err)
		return
	}
	if authentication != "" {
		encoded := base64.StdEncoding.EncodeToString([]byte(authentication))
		req.Header.Add("Authorization", "Basic "+encoded)
	}
	for _, h := range headers {
		i := strings.Index(h, ":")
		if i != -1 {
			req.Header.Add(h[0:i], h[i+1:])
		}
	}
	res, err := client.Do(req)
	if err != nil {
		slog.Error("fail to do request", "error", err)
		return
	}
	defer res.Body.Close()
	bytes, err := io.ReadAll(res.Body)
	if err != nil {
		slog.Error("fail to read response body", "error", err)
		return
	}
	slog.Info("webdav response", "resp_body", string(bytes), "status", res.StatusCode, "method", req.Method, "uri", req.RequestURI)
}
