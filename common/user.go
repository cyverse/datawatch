package common

import (
	"github.com/neo4j/neo4j-go-driver/neo4j"
)

// User - a user is simply a username and a list of emails
type User struct {
	// username of the user
	Username string `json:"username"`
	// email for the user
	Email string `json:"email,omitempty"`
	// admin flag
	Admin bool `json:"admin,omitempty"`
}

// Load - populate a User from a databse record
func (user *User) Load(record neo4j.Record) {
	if value, ok := record.Get("u.username"); ok {
		user.Username = value.(string)
	}
	if value, ok := record.Get("u.email"); ok {
		user.Email = value.(string)
	}
	if value, ok := record.Get("u.admin"); ok {
		if value != nil {
			user.Admin = value.(bool)
		}
	}
}

// LoadUser - return new user loaded from db
func LoadUser(username string) (*User, error) {
	var user User
	err := Load("MATCH (u:User {username:$username}) RETURN u.username,u.email,u.admin", map[string]interface{}{"username": username}, func(record neo4j.Record) {
		user.Load(record)
	})
	if err != nil {
		return nil, err
	}
	return &user, nil
}
