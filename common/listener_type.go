package common

import (
	"errors"
	"strings"
)

// ListenerType - base type for listeners
type ListenerType struct {
	// the system id for the listener type
	ListenerTypeID string `json:"listenerTypeID"`
	// This is the lowest interval (in seconds) for notifications for a data source. notification intervals cannot go lower than this.
	MinNotifyInterval int64 `json:"minNotifyInterval,omitempty"`
}

// EmailListenerType - type for email notifications
var EmailListenerType = ListenerType{
	ListenerTypeID:    "email",
	MinNotifyInterval: 60,
}

// WebDAVListenerType - type for email notifications
var WebDAVListenerType = ListenerType{
	ListenerTypeID:    "webdav",
	MinNotifyInterval: 60,
}

// WebhookListenerType - type for email notifications
var WebhookListenerType = ListenerType{
	ListenerTypeID:    "webhook",
	MinNotifyInterval: 60,
}

// Validate - checks details map for required parameters
func (l *ListenerType) Validate(details map[string]interface{}) error {
	if l == &EmailListenerType {
		return nil
	}
	method, ok := details["method"]
	if ok {
		method = strings.ToUpper(method.(string))
		if method != "GET" && method != "POST" && method != "PUT" {
			return errors.New("method other than GET, POST, or PUT specified")
		}
		details["method"] = method
	}
	url, ok := details["url"]
	if !ok || url == "" {
		return errors.New("required parameter url not specified")
	}
	return nil
}

// ListenerTypes - array of known types of listeners
var ListenerTypes = []*ListenerType{
	&EmailListenerType,
	&WebDAVListenerType,
	&WebhookListenerType,
}

// GetListenerType - return structure with matching ListenerTypeID
func GetListenerType(listenerTypeID string) *ListenerType {
	for _, v := range ListenerTypes {
		if v.ListenerTypeID == listenerTypeID {
			return v
		}
	}
	return nil
}
