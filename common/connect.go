package common

import (
	"log"
	"os"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
)

// NatsConnection - return connection to nats
func NatsConnection() *nats.Conn {
	url := os.Getenv("NATS_SERVICE_HOST")
	if url == "" {
		url = nats.DefaultURL
	}
	c, err := nats.Connect(url)
	if err != nil {
		log.Fatalf("Failed to connect to NATS at %s: %s", url, err)
	}
	return c
}

// StanConnection - return connection to nats streaming
func StanConnection(client, to string) stan.Conn {
	url := os.Getenv("NATS_SERVICE_HOST")
	if url == "" {
		url = nats.DefaultURL
	}
	clusterID := os.Getenv("clusterID")
	if clusterID == "" {
		clusterID = "stan"
	}
	c, err := stan.Connect(clusterID, "datawatch-"+client+"-"+to, stan.NatsURL(url))
	if err != nil {
		log.Fatalf("Failed to connect to NATS Streaming for datawatch-%s-%s: %s", client, to, err)
	}
	return c
}
