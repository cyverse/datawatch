package common

import (
	"fmt"
	"log"

	"github.com/neo4j/neo4j-go-driver/neo4j"
)

var driver neo4j.Driver

// InitDB - initialize neo4j driver
func InitDB() {
	uri := "bolt://172.16.11.95:7687"
	var err error
	driver, err = neo4j.NewDriver(uri, neo4j.BasicAuth("neo4j", "potami", ""), func(c *neo4j.Config) {
		c.Encrypted = false
	})
	if err != nil {
		log.Fatalf("Failed to init database: %s", err)
	}
}

// Execute = execute cypher code, nothing to return
func Execute(cypher string, args map[string]interface{}) error {
	session, err := driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return err
	}
	defer session.Close()

	_, err = session.Run(cypher, args)
	if err != nil {
		fmt.Println(cypher)
		fmt.Println(err)
	}
	return err
}

// Load - execute cypher code in neo4j, call func to popuplate struct
func Load(cypher string, args map[string]interface{}, f func(neo4j.Record)) error {
	// slog.Info(cypher)
	session, err := driver.Session(neo4j.AccessModeRead)
	if err != nil {
		return err
	}
	defer session.Close()

	result, err := session.Run(cypher, args)
	if err != nil {
		return err
	}

	for result.Next() {
		f(result.Record())
	}
	return nil
}
