package common

import (
	"strings"

	"github.com/neo4j/neo4j-go-driver/neo4j"
)

// Listener - connects a data source with a user and with config of how and when to publish events.
type Listener struct {
	Details        string   `json:"-"`                        // the settings particular to a listener type, stored as json string
	Enabled        bool     `json:"enabled,omitempty"`        // whether or not the listener is active
	EventFilters   string   `json:"eventFilters,omitempty"`   // comma delimited list of which event types to report
	ListenerID     string   `json:"listenerID"`               // the system generated id of a listener
	ListenerTypeID string   `json:"listenerTypeID"`           // indicates the type of listener, currently must be "email", "webdav", or "webhook"
	Name           string   `json:"name,omitempty"`           // optional name for the listener
	NotifyInterval int64    `json:"notifyInterval,omitempty"` // the notification interval (in seconds) for this listener. This cannot be lower than minNotifyInterval set by listener
	Paths          []string `json:"paths"`                    // the paths to listen to
	SourceID       string   `json:"sourceID"`                 // the data source id
	Username       string   `json:"username"`                 // the username who owns this listener
	eventFilters   []string
}

func getListeners(phrase string, m map[string]interface{}) ([]*Listener, error) {
	var listeners []*Listener
	var listener *Listener
	err := Load("MATCH "+phrase+" RETURN l.details,l.enabled,l.eventFilters,l.listenerID,l.name,l.notifyInterval,l.sourceID,l.listenerTypeID,p.path,u.username", m, func(record neo4j.Record) {
		if listener == nil {
			listener = new(Listener)
			listener.Load(record)
			return
		}
		if v, ok := record.Get("l.listenerID"); ok {
			if listener.ListenerID == v {
				if v, ok := record.Get("p.path"); ok {
					listener.Paths = append(listener.Paths, v.(string))
				}
			} else {
				listeners = append(listeners, listener)
				listener = new(Listener)
				listener.Load(record)
			}
		}
	})
	if err != nil {
		return nil, err
	}
	if listener != nil {
		listeners = append(listeners, listener)
	}
	return listeners, nil
}

// GetListeners - return listeners for a user
func GetListeners(username string) ([]*Listener, error) {
	return getListeners("(u:User {username:$username})-[:LISTENS_TO]->(l:Listener)-[:HAS_PATH]->(p)", map[string]interface{}{"username": username})
}

// GetListenersOnPath - return Listeners that are listening on path
func GetListenersOnPath(path string) ([]*Listener, error) {
	path2 := path
	if path2[len(path2)-1:] != "/" {
		path2 += "/"
	}
	return getListeners("(u:User)-[:LISTENS_TO]->(l:Listener)-[:HAS_PATH]->(p) WHERE l.enabled = true AND $path1 = p.path OR $path2 STARTS WITH p.path", map[string]interface{}{"path1": path, "path2": path2})
}

// Load ...
func (l *Listener) Load(record neo4j.Record) {
	if v, ok := record.Get("l.details"); ok {
		l.Details = v.(string)
	}
	if v, ok := record.Get("l.enabled"); ok {
		l.Enabled = v.(bool)
	}
	if v, ok := record.Get("l.eventFilters"); ok {
		if v != nil {
			l.EventFilters = v.(string)
			if l.EventFilters != "" {
				l.eventFilters = strings.Split(l.EventFilters, ",")
			}
		}
	}
	if v, ok := record.Get("l.listenerID"); ok {
		l.ListenerID = v.(string)
	}
	if v, ok := record.Get("l.listenerTypeID"); ok {
		l.ListenerTypeID = v.(string)
	}
	if v, ok := record.Get("l.name"); ok {
		if v != nil && v != "" {
			l.Name = v.(string)
		}
	}
	if v, ok := record.Get("l.notifyInterval"); ok {
		if v != nil {
			l.NotifyInterval = v.(int64)
		}
	}
	if v, ok := record.Get("l.sourceID"); ok {
		l.SourceID = v.(string)
	}
	if v, ok := record.Get("p.path"); ok {
		l.Paths = []string{v.(string)}
	}
	if v, ok := record.Get("u.username"); ok {
		l.Username = v.(string)
	}
}

func (l *Listener) EventMatches(event string) bool {
	if l.eventFilters == nil {
		return true
	}
	for _, f := range l.eventFilters {
		if f == event {
			return true
		}
	}
	return false
}

// LoadListener - return new Listener loaded from db
func LoadListener(listenerID string, username string) (*Listener, error) {
	first := true
	var listener Listener
	err := Load("MATCH (u:User {username:$username})-[:LISTENS_TO]-(l:Listener {listenerID:$listenerID})-[:HAS_PATH]->(p) RETURN l.details,l.enabled,l.eventFilters,l.listenerID,l.name,l.notifyInterval,l.sourceID,l.listenerTypeID,p.path,u.email", map[string]interface{}{"listenerID": listenerID, "username": username}, func(record neo4j.Record) {
		if first {
			listener.Load(record)
			listener.Username = username
			first = false
		} else if value, ok := record.Get("p.path"); ok {
			listener.Paths = append(listener.Paths, value.(string))
		}
	})
	if err != nil {
		return nil, err
	}
	if first {
		return nil, nil
	}
	return &listener, nil
}
