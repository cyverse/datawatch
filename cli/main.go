package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/mitchellh/cli"
	"gitlab.com/cyverse/datawatch/cli/command"
)

func main() {
	curl := flag.Bool("output-curl-string", false, "instead of executing the command, print the equivalent curl command")
	debug := flag.Bool("debug", false, "enable extra output when running commands")
	raw := flag.Bool("raw", false, "output JSON without formatting")
	flag.Parse()
	args := flag.Args()
	baseCommand := &command.BaseCommand{
		Debug:            *debug,
		OutputCurlString: *curl,
		Raw:              *raw,
		UI: &cli.ColoredUi{
			InfoColor:  cli.UiColorBlue,
			ErrorColor: cli.UiColorRed,
			Ui: &cli.BasicUi{
				Writer:      os.Stdout,
				ErrorWriter: os.Stderr,
				Reader:      os.Stdin,
			},
		},
	}
	cli := &cli.CLI{
		Args:     args,
		Commands: Commands(baseCommand),
		Version:  Name + " version " + Version,
		HelpFunc: func(cmds map[string]cli.CommandFactory) string {
			additionalOutput := `
Universal flags:
    -debug                      enable extra output when running commands
    -output-curl-string         instead of executing the command, print the equivalent curl command
    -raw                        output JSON without formatting
`
			return cli.BasicHelpFunc(Name)(cmds) + additionalOutput
		},
		HelpWriter: os.Stdout,
	}

	exitCode, err := cli.Run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to execute: %s\n", err.Error())
	}
	os.Exit(exitCode)
}
