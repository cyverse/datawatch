package main

import (
	"github.com/mitchellh/cli"
	"gitlab.com/cyverse/datawatch/cli/command"
)

// Commands holds a map of all possible commands
func Commands(baseCommand *command.BaseCommand) map[string]cli.CommandFactory {
	return map[string]cli.CommandFactory{
		"create": func() (cli.Command, error) {
			return &command.CreateCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"create listener": func() (cli.Command, error) {
			return &command.CreateListenerCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"create user": func() (cli.Command, error) {
			return &command.CreateUserCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"delete": func() (cli.Command, error) {
			return &command.DeleteCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"delete listener": func() (cli.Command, error) {
			return &command.DeleteListenerCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"delete user": func() (cli.Command, error) {
			return &command.DeleteUserCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"get": func() (cli.Command, error) {
			return &command.GetCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"get accessToken": func() (cli.Command, error) {
			return &command.GetAccessTokenCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"get dataSources": func() (cli.Command, error) {
			return &command.GetDataSourcesCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"get db": func() (cli.Command, error) {
			return &command.GetDbCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"get listeners": func() (cli.Command, error) {
			return &command.GetListenersCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"get listenerTypes": func() (cli.Command, error) {
			return &command.GetListenerTypesCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"get state": func() (cli.Command, error) {
			return &command.GetStateCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"get users": func() (cli.Command, error) {
			return &command.GetUsersCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"login": func() (cli.Command, error) {
			return &command.LoginCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"trigger": func() (cli.Command, error) {
			return &command.TriggerCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"trigger listener": func() (cli.Command, error) {
			return &command.TriggerListenerCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"update": func() (cli.Command, error) {
			return &command.UpdateCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"update listener": func() (cli.Command, error) {
			return &command.UpdateListenerCommand{
				BaseCommand: baseCommand,
			}, nil
		},
		"update user": func() (cli.Command, error) {
			return &command.UpdateUserCommand{
				BaseCommand: baseCommand,
			}, nil
		},
	}
}
