package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// GetUsersCommand ...
type GetUsersCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetUsersCommand) Run(args []string) int {
	if len(args) > 1 {
		c.UI.Error(c.Help())
		return 1
	}
	path := "users"
	if len(args) == 1 {
		path += "/" + args[0]
	}
	u := api.URL(path)
	code, _ := c.DoCommand("GET", u)
	return code
}

// Synopsis ...
func (c *GetUsersCommand) Synopsis() string {
	return "get users"
}

// Help ...
func (c *GetUsersCommand) Help() string {
	helpText := `
Returns all the users unless USERNAME is provided. Only Admins can get all users.

Usage: dw get users [USERNAME]
`
	return strings.TrimSpace(helpText)
}
