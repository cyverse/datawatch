package command

import (
	"flag"
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// GetListenersCommand ...
type GetListenersCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetListenersCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	var username string
	flagSet.StringVar(&username, "username", "", "the username for the user whose listeners should be returned (admins only)")
	flagSet.StringVar(&username, "u", "", "the username for the user whose listeners should be returned (admins only) (shorthand)")
	flagSet.Parse(args)
	args = flagSet.Args()
	if len(args) > 1 {
		c.UI.Error(c.Help())
		return 1
	}
	path := "listeners"
	if len(args) == 1 {
		path += "/" + args[0]
	}
	u := api.URL(path)
	if username != "" {
		q := u.Query()
		q.Add("username", username)
		u.RawQuery = q.Encode()
	}
	code, _ := c.DoCommand("GET", u)
	return code
}

// Synopsis ...
func (c *GetListenersCommand) Synopsis() string {
	return "get listeners for the requesting user"
}

// Help ...
func (c *GetListenersCommand) Help() string {
	helpText := `
Returns all the listeners for the requesting user unless LISTENER_ID is provided

Usage: dw get listeners [options] [LISTENER_ID]

Options:
    -u, -username <string>
        can be specified by admins to fetch the listeners for a specific user
`
	return strings.TrimSpace(helpText)
}
