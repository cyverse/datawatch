package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// TriggerListenerCommand ...
type TriggerListenerCommand struct {
	*BaseCommand
}

// Run ...
func (c *TriggerListenerCommand) Run(args []string) int {
	if len(args) != 2 {
		c.UI.Error(c.Help())
		return 1
	}
	u := api.URL("listeners/" + args[0])
	q := u.Query()
	q.Add("data", args[1])
	u.RawQuery = q.Encode()
	code, _ := c.DoCommand("POST", u)
	return code
}

// Synopsis ...
func (c *TriggerListenerCommand) Synopsis() string {
	return "trigger a listener to publish a test notification containing the supplied data"
}

// Help ...
func (c *TriggerListenerCommand) Help() string {
	helpText := `
Usage: dw trigger listener LISTENER_ID DATA
`
	return strings.TrimSpace(helpText)
}
