package command

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"

	"github.com/fatih/color"
	prettyjson "github.com/hokaccha/go-prettyjson"
	"github.com/mitchellh/cli"
	"gitlab.com/cyverse/datawatch/cli/login"
)

// BaseCommand ...
type BaseCommand struct {
	Debug            bool
	OutputCurlString bool
	Raw              bool
	UI               cli.Ui
}

var formatter = prettyjson.Formatter{
	KeyColor:        color.New(color.FgBlue, color.Bold),
	StringColor:     color.New(color.FgGreen, color.Bold),
	BoolColor:       color.New(color.FgYellow, color.Bold),
	NumberColor:     color.New(color.FgCyan, color.Bold),
	NullColor:       color.New(color.FgBlack, color.Bold),
	StringMaxLength: 0,
	DisabledColor:   false,
	Indent:          4,
	Newline:         "\n",
}

// DoCommand will either execute a Request or print the curl command, return code for Command.Run()
func (c *BaseCommand) DoCommand(method string, u *url.URL) (int, []byte) {
	c.printDebug("URL:%s\n", u.String())
	req, err := http.NewRequest(method, u.String(), nil)
	if err != nil {
		log.Println("NewRequest error for " + method + " command")
		return 1, nil
	}
	token := login.GetKeycloakToken("", "")
	if token == "" {
		return 1, nil
	}
	req.Header.Add("Authorization", token)
	if c.OutputCurlString {
		c.printCurlString(req)
		return 0, nil
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		e := err.Error()
		if strings.HasSuffix(e, "for key Authorization") {
			return c.DoCommand(method, u)
		}
		fmt.Println(e)
		return 1, nil
	}
	if resp.StatusCode == 403 {
		fmt.Println("keycloak token has expired")
		os.Remove(login.KeycloakTokenFile())
		return c.DoCommand(method, u)
	}
	c.printDebug("Response struct:\n%v\n", resp)
	return 0, c.printHTTPResponse(resp)
}

// printCurlString will output a string that is the equivalent curl command for
// the specified request. This will be helpful for debugging. A majority of the
// code in this function comes from Hashicorp Vault's equivalent function:
// https://github.com/hashicorp/vault/blob/528604359c20a32fef628d86abdb7663e81235c3/api/output_string.go#L35-L64
func (c *BaseCommand) printCurlString(req *http.Request) {
	result := "curl"
	result += fmt.Sprintf(" -X %s", req.Method)
	for k, v := range req.Header {
		for _, h := range v {
			result += fmt.Sprintf(` -H "%s: %s"`, k, h)
		}
	}

	var body []byte
	var err error
	if req.Body != nil {
		body, err = io.ReadAll(req.Body)
		if err != nil {
			c.UI.Error("Unable to parse request body when constructing curl string")
			return
		}
		req.Body = io.NopCloser(bytes.NewBuffer(body))
	}

	if len(body) > 0 {
		removeWhitespace := regexp.MustCompile(`[\t\n]`)
		escapedBody := string(removeWhitespace.ReplaceAll(body, []byte("")))
		escapedBody = strings.Replace(escapedBody, "'", "'\"'\"'", -1)
		result += fmt.Sprintf(" -d '%s'", escapedBody)
	}

	result += fmt.Sprintf(" %s", req.URL.String())
	c.UI.Output(result)
}

// printDebug is used to print some output only when the debug flag is used
func (c *BaseCommand) printDebug(msg string, extras ...interface{}) {
	if c.Debug {
		c.UI.Output(fmt.Sprintf(msg, extras...))
	}
}

// printHTTPResponse is used to print the body and error (if it exists) from an HTTP Response
func (c *BaseCommand) printHTTPResponse(response *http.Response) []byte {
	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
		return nil
	}
	if response.StatusCode >= 300 {
		c.UI.Error(string(body))
		return nil
	}
	if c.Raw {
		c.UI.Output(string(body))
		return body
	}
	if len(body) > 0 {
		var i interface{}
		json.Unmarshal(body, &i)
		json, err := formatter.Marshal(i)
		if err != nil {
			log.Println(err)
			return nil
		}
		c.UI.Output(string(json))
	}
	return body
}
