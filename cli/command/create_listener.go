package command

import (
	"flag"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// CreateListenerCommand ...
type CreateListenerCommand struct {
	*BaseCommand
}

type stringFlags []string

func (i *stringFlags) Set(value string) error {
	*i = append(*i, strings.TrimSpace(value))
	return nil
}

// this is just to satisfy the interface
func (i *stringFlags) String() string {
	return ""
}

func appendParams(u *url.URL, args []string) {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	var authentication string
	flagSet.StringVar(&authentication, "authentication", "", "Basic authentication string if needed, must be in the form \"username:password\"")
	flagSet.StringVar(&authentication, "a", "", "Basic authentication string if needed, must be in the form \"username:password\" (shorthand)")
	var email string
	flagSet.StringVar(&email, "email", "", "email address to send notifications to (user's email will be used if this is not specified)")
	flagSet.StringVar(&email, "e", "", "email address to send notifications to (user's email will be used if this is not specified) (shorthand)")
	var enabled bool
	flagSet.BoolVar(&enabled, "enabled", true, "whether or not the listener is active, note: to disable, the format -enabled=false is required")
	var eventFilters string
	flagSet.StringVar(&eventFilters, "eventFilter", "", "event type to report on")
	flagSet.StringVar(&eventFilters, "f", "", "event type to report on")
	var headers stringFlags
	flagSet.Var(&headers, "header", "HTTP headers for webhook notifications")
	flagSet.Var(&headers, "h", "HTTP headers for webhook notifications (shorthand)")
	var listenerTypeID string
	flagSet.StringVar(&listenerTypeID, "listenerTypeID", "", "the kind of listener to create; 'email', 'webdav', or 'webhook'")
	flagSet.StringVar(&listenerTypeID, "t", "", "the kind of listener to create; 'email', 'webdav', or 'webhook' (shorthand)")
	var method string
	flagSet.StringVar(&method, "method", "", "HTTP method to use for webhook notifications; 'GET', 'POST', or 'PUT'")
	flagSet.StringVar(&method, "m", "", "HTTP method to use for webhook notifications; 'GET', 'POST', or 'PUT' (shorthand)")
	var name string
	flagSet.StringVar(&name, "name", "", "optional name for the listener")
	flagSet.StringVar(&name, "n", "", "optional name for the listener (shorthand)")
	var notifyInterval int
	flagSet.IntVar(&notifyInterval, "notifyInterval", 0, "seconds to hold for bundling with future events")
	flagSet.IntVar(&notifyInterval, "i", 0, "seconds to hold for bundling with future events (shorthand)")
	var params stringFlags
	flagSet.Var(&params, "param", "HTTP form params for webhook notifications")
	flagSet.Var(&params, "p", "HTTP form params for webhook notifications (shorthand)")
	var sourceID string
	flagSet.StringVar(&sourceID, "sourceID", "", "ID of data source to monitor")
	flagSet.StringVar(&sourceID, "s", "", "ID of data source to monitor (shorthand)")
	var url string
	flagSet.StringVar(&url, "url", "", "URL for webdav and webhook listeners")
	flagSet.StringVar(&url, "u", "", "URL for webdav and webhook listeners (shorthand)")
	var username string
	flagSet.StringVar(&username, "username", "", "assign listener to specified user (admin-only)")
	flagSet.Parse(args)
	args = flagSet.Args()
	q := u.Query()
	for _, p := range args {
		q.Add("path", p)
	}
	if authentication != "" {
		q.Add("authentication", authentication)
	}
	if email != "" {
		q.Add("email", email)
	}
	if enabled {
		q.Add("enabled", "true")
	} else {
		q.Add("enabled", "false")
	}
	if eventFilters != "" {
		q.Add("eventFilters", eventFilters)
	}
	for _, h := range headers {
		q.Add("header", h)
	}
	if listenerTypeID != "" {
		q.Add("listenerTypeID", listenerTypeID)
	}
	if method != "" {
		q.Add("method", method)
	}
	if name != "" {
		q.Add("name", name)
	}
	if notifyInterval != 0 {
		q.Add("notifyInterval", strconv.Itoa(notifyInterval))
	}
	for _, p := range params {
		q.Add("param", p)
	}
	if sourceID != "" {
		q.Add("sourceID", sourceID)
	}
	if url != "" {
		q.Add("url", url)
	}
	if username != "" {
		q.Add("username", username)
	}
	u.RawQuery = q.Encode()
}

// Run ...
func (c *CreateListenerCommand) Run(args []string) int {
	u := api.URL("listeners")
	appendParams(u, args)
	q := u.Query()
	if q.Get("listenerTypeID") == "" || q.Get("sourceID") == "" || q.Get("path") == "" {
		c.UI.Error(c.Help())
		return 1
	}
	code, _ := c.DoCommand("POST", u)
	return code
}

// Synopsis ...
func (c *CreateListenerCommand) Synopsis() string {
	return "create a new listener"
}

// Help ...
func (c *CreateListenerCommand) Help() string {
	helpText := `
Usage: dw create listener [options] path [path ...]

one or more path arguments must be specified

Note: -listenerTypeID (or -t) and -sourceID (or -s) are both required. -url (or -u) is required for WebDAV and Webhook listeners.

Options:
    -enabled true|false
        whether or not the listener is active, note: to disable, the format -enabled=false is required
    -f, -eventFilters created|modified|moved|removed
        optional event type to report on
        this option may be specified zero or more times
    -t, -listenerTypeID <string>
        (required)
        the kind of listener to create, 'email', 'webdav', or 'webhook'
    -n, -name <string>
        name for the listener
    -i, -notifyInterval <integer>
        seconds to hold for bundling with future events
        if not supplied, a default will be used
    -s, -sourceID <string>
        (required)
        ID of data source to monitor
    -username <string>
        assign listener to specified user, only admins can set this option
EMail options:
    -e, -email <string>
        email address to send notifications to (user's email will be used if this is not specified)
WebDAV and Webhook options:
    -a, -authentication <string>
        authentication string to use if Basic authentication is needed, must be in form "username:password"
    -h, -header <string>
        optional HTTP header to use for notification
        this option may be specified zero or more times
WebDAV options:
    -u, -url <string>
        (required)
        URL destination for notifications file, it is assumed to represent a directory, the filename will be automatically generated
Webhook options:
    -m -method <string>
        the HTTP method to use; 'GET', 'POST', or 'PUT'
        if not supplied, GET is the default
    -p, -param <string> (in name=value format)
        optional form parameters; if one or more are specified, method will be set to POST and Content-Type will be set to multipart/form-data
    -u, -url <string>
        (required)
        URL destination for notifications, if a scheme is not included, http:// will be used
`
	return strings.TrimSpace(helpText)
}
