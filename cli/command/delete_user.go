package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// DeleteUserCommand ...
type DeleteUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteUserCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error(c.Help())
		return 1
	}
	u := api.URL("users/" + args[0])
	code, _ := c.DoCommand("DELETE", u)
	return code
}

// Synopsis ...
func (c *DeleteUserCommand) Synopsis() string {
	return "delete a user"
}

// Help ...
func (c *DeleteUserCommand) Help() string {
	helpText := `
Usage: dw delete user USERNAME
`
	return strings.TrimSpace(helpText)
}
