package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// DeleteListenerCommand ...
type DeleteListenerCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteListenerCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error(c.Help())
		return 1
	}
	u := api.URL("listeners/" + args[0])
	code, _ := c.DoCommand("DELETE", u)
	return code
}

// Synopsis ...
func (c *DeleteListenerCommand) Synopsis() string {
	return "delete a listener"
}

// Help ...
func (c *DeleteListenerCommand) Help() string {
	helpText := `
Usage: dw delete listener LISTENER_ID
`
	return strings.TrimSpace(helpText)
}
