package command

import (
	"flag"
	"os"
	"strings"

	"gitlab.com/cyverse/datawatch/cli/login"
)

// LoginCommand ...
type LoginCommand struct {
	*BaseCommand
}

// Run ...
func (c *LoginCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	var username string
	flagSet.StringVar(&username, "username", "", "username for obtaining keycloak token")
	flagSet.StringVar(&username, "u", "", "username for obtaining keycloak token (shorthand)")
	var password string
	flagSet.StringVar(&password, "password", "", "password for obtaining keycloak token")
	flagSet.StringVar(&password, "p", "", "password for obtaining keycloak token (shorthand)")
	flagSet.Parse(args)
	os.Remove(login.KeycloakTokenFile())
	login.GetKeycloakToken(username, password)
	return 0
}

// Synopsis ...
func (c *LoginCommand) Synopsis() string {
	return "fetch a keycloak token and store it for future use"
}

// Help ...
func (c *LoginCommand) Help() string {
	helpText := `
this command can be used to fetch a keycloak token. NOTE: normally this command is not needed, the CLI will prompt for a username and password whenever necessary.

Usage: dw login [(-u | -username) <USERNAME>] [(-p | -password) <PASSWORD>]

Options:
    -u, -username
        specify username on command line, if not supplied you will be prompted to enter it
    -p, -password
        specify password on command line, if not supplied you will be prompted to enter it
`
	return strings.TrimSpace(helpText)
}
