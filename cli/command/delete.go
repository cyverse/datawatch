package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// DeleteCommand ...
type DeleteCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *DeleteCommand) Synopsis() string {
	return "delete a listener or user"
}

// Help ...
func (c *DeleteCommand) Help() string {
	helpText := `
Usage: dw delete <subcommand>
`
	return strings.TrimSpace(helpText)
}
