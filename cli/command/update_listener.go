package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// UpdateListenerCommand ...
type UpdateListenerCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateListenerCommand) Run(args []string) int {
	if len(args) == 0 {
		c.UI.Error(c.Help())
		return 1
	}
	u := api.URL("listeners/" + args[0])
	appendParams(u, args[1:])
	code, _ := c.DoCommand("PUT", u)
	return code
}

// Synopsis ...
func (c *UpdateListenerCommand) Synopsis() string {
	return "update a listener"
}

// Help ...
func (c *UpdateListenerCommand) Help() string {
	helpText := `
Usage: dw update listener LISTENER_ID [options] [path ...]

one or more path arguments may be specified but are not required. All options are optional.

Options:
    -enabled true|false
        whether or not the listener is active, note: to disable, the format -enabled=false is required
    -f, -eventFilters created|modified|moved|removed
        optional event type to report on
        this option may be specified zero or more times
    -t, -listenerTypeID <string>
        the kind of listener, 'email', 'webdav', or 'webhook'
    -n, -name <string>
        name for the listener
    -i, -notifyInterval <integer>
        seconds to hold for bundling with future events
    -s, -sourceID <string>
        ID of data source to monitor
EMail options:
    -e, -email <string>
        email address to send notifications to (user's email will be used if this is not specified)
WebDAV and Webhook options:
    -a, -authentication <string>
        authentication string to use if Basic authentication is needed, must be in form "username:password"
    -h, -header <string>
        optional HTTP header to use for notification
        this option may be specified zero or more times
WebDAV options:
    -u, -url <string>
        (required)
        URL destination for notifications file, it is assumed to represent a directory, the filename will be automatically generated
Webhook options:
    -m -method <string>
        the HTTP method to use; 'GET', 'POST', or 'PUT'
        if not supplied, GET is the default
    -p, -param <string> (in name=value format)
        optional form parameters; if one or more are specified then the method will be set to POST and Content-Type will be set to multipart/form-data
    -u, -url <string>
        (required)
        URL destination for notifications, if a scheme is not included, http:// will be used
`
	return strings.TrimSpace(helpText)
}
