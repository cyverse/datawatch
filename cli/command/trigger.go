package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// TriggerCommand ...
type TriggerCommand struct {
	*BaseCommand
}

// Run ...
func (c *TriggerCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *TriggerCommand) Synopsis() string {
	return "trigger a listener"
}

// Help ...
func (c *TriggerCommand) Help() string {
	helpText := `
Usage: dw trigger <subcommand>
`
	return strings.TrimSpace(helpText)
}
