package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// GetCommand ...
type GetCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *GetCommand) Synopsis() string {
	return "fetch data sources, listeners, listener types, personal access token, state, or users"
}

// Help ...
func (c *GetCommand) Help() string {
	helpText := `
Usage: dw get <subcommand>
`
	return strings.TrimSpace(helpText)
}
