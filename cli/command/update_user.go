package command

import (
	"flag"
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// UpdateUserCommand ...
type UpdateUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateUserCommand) Run(args []string) int {
	if len(args) == 0 {
		c.UI.Error(c.Help())
		return 1
	}
	username := args[0]
	if username[0] == '-' {
		c.UI.Error(c.Help())
		return 1
	}
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	var admin bool
	flagSet.BoolVar(&admin, "admin", false, "update user with admin permissions")
	flagSet.BoolVar(&admin, "a", false, "update user with admin permissions (shorthand)")
	var email string
	flagSet.StringVar(&email, "email", "", "update user with new email address")
	flagSet.StringVar(&email, "e", "", "update user with new email address (shorthand)")
	flagSet.Parse(args[1:])
	c.printDebug("Parsed command line flags:\n  username: %s\n  admin: %t\n  email: %s\n", username, admin, email)
	u := api.URL("users/" + username)
	q := u.Query()
	if email != "" {
		q.Add("email", email)
	}
	if admin {
		q.Add("admin", "true")
	}
	u.RawQuery = q.Encode()
	code, _ := c.DoCommand("PUT", u)
	return code
}

// Synopsis ...
func (c *UpdateUserCommand) Synopsis() string {
	return "update a user"
}

// Help ...
func (c *UpdateUserCommand) Help() string {
	helpText := `
Usage: dw update user USERNAME [options]

Options:
    -a, -admin
        update user with admin permissions
    -e, -email
        update user with new email address
`
	return strings.TrimSpace(helpText)
}
