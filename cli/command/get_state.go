package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// GetStateCommand ...
type GetStateCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetStateCommand) Run(args []string) int {
	if len(args) > 0 {
		c.UI.Error(c.Help())
		return 1
	}
	u := api.URL("state")
	code, _ := c.DoCommand("GET", u)
	return code
}

// Synopsis ...
func (c *GetStateCommand) Synopsis() string {
	return "get state"
}

// Help ...
func (c *GetStateCommand) Help() string {
	helpText := `
Returns the current state and stats of the DataWatch services. Only Admins can get state.

Usage: dw get state
`
	return strings.TrimSpace(helpText)
}
