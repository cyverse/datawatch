package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// GetDbCommand ...
type GetDbCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetDbCommand) Run(args []string) int {
	u := api.URL("db")
	code, _ := c.DoCommand("GET", u)
	return code
}

// Synopsis ...
func (c *GetDbCommand) Synopsis() string {
	return "get db"
}

// Help ...
func (c *GetDbCommand) Help() string {
	helpText := `
Get database contents (admins only)

Usage: dw get db
`
	return strings.TrimSpace(helpText)
}
