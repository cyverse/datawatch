package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
	"gitlab.com/cyverse/datawatch/cli/personalaccess"
)

// GetAccessTokenCommand ...
type GetAccessTokenCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetAccessTokenCommand) Run(args []string) int {
	u := api.URL("accessToken")
	code, body := c.DoCommand("GET", u)
	personalaccess.WriteAccessToken(string(body))
	return code
}

// Synopsis ...
func (c *GetAccessTokenCommand) Synopsis() string {
	return "fetch a personal access token and store it for future use"
}

// Help ...
func (c *GetAccessTokenCommand) Help() string {
	helpText := `
this command can be used to fetch a personal access token and store it for future use

Usage: dw get accesstoken
`
	return strings.TrimSpace(helpText)
}
