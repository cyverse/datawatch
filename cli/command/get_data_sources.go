package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// GetDataSourcesCommand ...
type GetDataSourcesCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetDataSourcesCommand) Run(args []string) int {
	u := api.URL("dataSources")
	code, _ := c.DoCommand("GET", u)
	return code
}

// Synopsis ...
func (c *GetDataSourcesCommand) Synopsis() string {
	return "get data sources"
}

// Help ...
func (c *GetDataSourcesCommand) Help() string {
	helpText := `
Usage: dw get dataSources
`
	return strings.TrimSpace(helpText)
}
