package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// CreateCommand ...
type CreateCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *CreateCommand) Synopsis() string {
	return "create a listener or user"
}

// Help ...
func (c *CreateCommand) Help() string {
	helpText := `
Usage: dw create <subcommand> [options]
`
	return strings.TrimSpace(helpText)
}
