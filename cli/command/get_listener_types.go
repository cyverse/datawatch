package command

import (
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// GetListenerTypesCommand ...
type GetListenerTypesCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetListenerTypesCommand) Run(args []string) int {
	u := api.URL("listenerTypes")
	code, _ := c.DoCommand("GET", u)
	return code
}

// Synopsis ...
func (c *GetListenerTypesCommand) Synopsis() string {
	return "get listener types"
}

// Help ...
func (c *GetListenerTypesCommand) Help() string {
	helpText := `
Usage: dw get listenerTypes
`
	return strings.TrimSpace(helpText)
}
