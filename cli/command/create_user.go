package command

import (
	"flag"
	"strings"

	"gitlab.com/cyverse/datawatch/cli/api"
)

// CreateUserCommand ...
type CreateUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateUserCommand) Run(args []string) int {
	if len(args) < 2 {
		c.UI.Error(c.Help())
		return 1
	}
	username := args[0]
	email := args[1]
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	var admin bool
	flagSet.BoolVar(&admin, "admin", false, "give new user admin permissions")
	flagSet.BoolVar(&admin, "a", false, "give new user admin permissions (shorthand)")
	flagSet.Parse(args[2:])
	c.printDebug("Parsed command line flags:\n  username: '%s'\n  email: '%s'\n  admin: %t\n", username, email, admin)
	if len(username) == 0 || len(email) == 0 {
		c.UI.Error(c.Help())
		return 1
	}
	u := api.URL("users")
	q := u.Query()
	q.Add("username", username)
	q.Add("email", email)
	if admin {
		q.Add("admin", "true")
	}
	u.RawQuery = q.Encode()
	code, _ := c.DoCommand("POST", u)
	return code
}

// Synopsis ...
func (c *CreateUserCommand) Synopsis() string {
	return "create a new user"
}

// Help ...
func (c *CreateUserCommand) Help() string {
	helpText := `
Usage: dw create user USERNAME EMAIL [options]

Options:
    -a, -admin
        give new user admin permissions
`
	return strings.TrimSpace(helpText)
}
