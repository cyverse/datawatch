package api

import (
	"net/url"
	"os"
	"strings"
)

// URL return URL for API request
func URL(path string) *url.URL {
	var host string
	var scheme string
	u := os.Getenv("DATAWATCH_API")
	if u == "" {
		scheme = "http"
		host = "localhost"
	} else {
		index := strings.Index(u, "://")
		if index == -1 {
			scheme = "http"
			host = u
		} else {
			scheme = u[:index]
			host = u[index+3:]
		}
	}
	return &url.URL{
		Scheme: scheme,
		Host:   host,
		Path:   path,
	}
}
