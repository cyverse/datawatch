# DataWatch CLI

### Building

Build using:

```bash
go build -o datawatch
```

Then, use according to usage instructions from `./datawatch` and `./datawatch <subcommand>`

Before using commands, you need to define the following environment variable:
  - `DATAWATCH_API` - base URL for API
    - default: http://localhost
    - Note: if this does not start with the scheme, `http://` will be prepended so if `https` is required, make sure it is included in the variable
For example:
```bash
export DATAWATCH_API=https://datawatch.cyverse.org
```
A line setting the environment variable can be added to your shell's profile or config file. For bash add it to ~/.bashrc

### Authentication
The DataWatch API uses keycloak for authentication by default. So the CLI needs to obtain a keycloak token in order to make calls to the API. The first time you run the CLI you will be prompted to enter your keycloak username and password. They will be used to obtain a token which is then written to a file for running the CLI in the same session. The token will expire over time and so the CLI will prompt you again to enter your username and password when that happens. If for some reason you want to manually obtain a new keycloak token, run `./datawatch login`.

### Admin Users
If you are set as an admin in keycloak and/or there already exists a user in DataWatch with your username and admin privileges then you are a DataWatch admin. Only admins can create users, update users, and delete users. Additionally, when admins use the `get users` command, all users known to DataWatch are returned. For regular users, that command only returns their own user record.

### Examples
```bash
./datawatch create user <username> <email> [-admin]
```
Adding the optional `-admin` flag at the end when creating a new user gives them admin privileges.

Here's an example of an email listener
```bash
./datawatch create listener -listenerTypeID=email -sourceID=cyverse "/iplant/home/<username>/analyses" "/iplant/home/<username>/test"
```
> Note this command takes one or more arguments (after options), each being a path that the listener will use when matching events

Here's an example of a webdav listener. The notification will use WEBDAV to put the notification text in a file on the CyVerse data store.
```bash
./datawatch create listener -listenerTypeID webdav -sourceID cyverse -url https://data.cyverse.org/dav/iplant/home/<username>/event.json -username <username> -password <password> /iplant/home/<username>/test
```

Here's an example of webhook listener. The notification will hit a `POST` endpoint with `Authorization` header.
```bash
./datawatch create listener -listenerTypeID webhook -sourceID cyverse \
  -m POST -header 'Authorization: <what-ever-token-you-need>' \
  -u "https://example.com/datawatch-upload" \
  -notifyInterval 5 \
  /iplant/home/<username>/watched-folder
```

### Command Options
The required `-listenerTypeID` option must be one of `email`, `webdav`, or `webhook`. The required  `-sourceID` option must currently be `cyverse`. There is also the `-notifyInterval` option which is not required and can be used to override the default interval used for bundling events for a given path.

### Additional commands
`delete listener`, `delete user`, `get dataSources`, `get keycloakToken`, `get listeners`, `get listenerTypes`, `get users`, `update listener`, and `update user`.

### Development

To add a new command, you first need to add a new file in `command/` and follow the example of other files. Then, add the struct to the map in `commands.go`.
