package login

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"syscall"

	"gitlab.com/cyverse/datawatch/cli/api"
	"golang.org/x/term"
)

func get(u *url.URL) string {
	fmt.Println("\nconnecting to " + u.Scheme + "://" + u.Host + "...")
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return ""
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return ""
	}
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return ""
	}
	return string(b)
}

// GetKeycloakToken - return token from config file
func GetKeycloakToken(username string, password string) string {
	token := os.Getenv("DATAWATCH_KEYCLOAKTOKEN")
	if token != "" {
		return token
	}

	f := KeycloakTokenFile()
	b, err := os.ReadFile(f)
	if err == nil {
		return string(b)
	}

	u := api.URL("keycloakToken")
	q := u.Query()
	if username == "" {
		username = getUsername()
	}
	q.Add("username", username)
	if password == "" {
		password = getPassword()
	}
	q.Add("password", password)
	u.RawQuery = q.Encode()
	token = get(u)
	if token == "" {
		fmt.Println("unable to fetch keycloak token")
		return ""
	}
	token = token[1 : len(token)-1] // strip off JSON double quotes

	os.WriteFile(f, []byte(token), 0600)
	return token
}

func getPassword() string {
	fmt.Print("Enter your password: ")
	b, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		fmt.Println("problem reading password")
		return ""
	}
	password := string(b)
	if password == "" {
		log.Fatal("password required")
	}
	return password
}

func getUsername() string {
	fmt.Print("Enter your keycloak username: ")
	var username string
	fmt.Scanln(&username)
	if username == "" {
		log.Fatal("username required")
	}
	return username
}

// KeycloakTokenFile - return the path for the token file
func KeycloakTokenFile() string {
	d, _ := os.UserConfigDir()
	os.MkdirAll(d, 0755)
	return filepath.Join(d, ".datawatch_keycloaktoken")
}
