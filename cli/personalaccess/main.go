package personalaccess

import (
	"os"
	"path/filepath"
)

// AccessTokenFile - return the path for the token file
func AccessTokenFile() string {
	d, _ := os.UserConfigDir()
	os.MkdirAll(d, 0755)
	return filepath.Join(d, ".datawatch_accesstoken")
}

// GetAccessToken - return token from config file
func GetAccessToken() string {
	token := os.Getenv("DATAWATCH_ACCESSTOKEN")
	if token != "" {
		return token
	}

	f := AccessTokenFile()
	b, err := os.ReadFile(f)
	if err != nil {
		return ""
	}
	return string(b)
}

// WriteAccessToken -
func WriteAccessToken(token string) {
	f := AccessTokenFile()
	os.WriteFile(f, []byte(token), 0600)
}
