package main

import (
	"encoding/json"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"strconv"

	irodsfs_client "github.com/cyverse/go-irodsclient/fs"
	irodsfs_clienttype "github.com/cyverse/go-irodsclient/irods/types"
	"github.com/nats-io/nats.go"
	"gitlab.com/cyverse/datawatch/common"
)

var account = irodsfs_clienttype.IRODSAccount{
	AuthenticationScheme:    irodsfs_clienttype.AuthSchemeNative,
	ClientServerNegotiation: false,
	CSNegotiationPolicy:     irodsfs_clienttype.CSNegotiationRequireTCP,
	Host:                    os.Getenv("ihost"),
	Port:                    atoi(os.Getenv("iport")),
	ClientUser:              os.Getenv("iuser"),
	ClientZone:              os.Getenv("izone"),
	ProxyUser:               os.Getenv("iuser"),
	ProxyZone:               os.Getenv("izone"),
	Password:                os.Getenv("ipw"),
	PamTTL:                  irodsfs_clienttype.PamTTLDefault,
	SSLConfiguration:        nil,
}

var fsclient *irodsfs_client.FileSystem

// check - struct holding username, path, and permissions
type check struct {
	username    string
	path        string
	permissions chan permissions
}

// permissions - iRODS access level string and bool indicating path is readable
type permissions struct {
	AccessLevelString string `json:"accessLevelString"`
	Readable          bool   `json:"readable"`
}

var checks chan check

func main() {
	var err error
	fsclient, err = irodsfs_client.NewFileSystemWithDefault(&account, "datawatch-perm-service")
	if err != nil {
		log.Fatalf("%s: %s", "Error connecting to iRODS", err)
	}
	defer fsclient.Release()

	monitorConnection := common.NatsConnection()
	defer monitorConnection.Close()

	const numWorkers = 10
	checks = make(chan check, numWorkers)
	for i := 0; i < numWorkers; i++ {
		go worker(checks)
	}

	sub, err := monitorConnection.Subscribe("datawatch.permissions", func(msg *nats.Msg) {
		slog.Info("perm-service received msg", "subject", msg.Subject, "queue", msg.Sub.Queue, "pid", os.Getpid(), "msgData", string(msg.Data))
		check := makeCheck(msg)
		checks <- check
		permissions := <-check.permissions
		json, _ := json.Marshal(permissions)
		// slog.Info("Sending msg", "json", string(json))
		msg.Respond(json)
	})
	if err != nil {
		log.Fatalf("Problem subscribing to datawatch.permissions: %s", err)
	}
	defer sub.Unsubscribe()

	slog.Info("perm-service listening on subject datawatch.permissions, press ctrl+c to exit")
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	slog.Info("exiting perm service")
}

func atoi(port string) int {
	i, err := strconv.Atoi(port)
	if err != nil {
		log.Fatal(err)
	}
	return i
}

func checkPath(username string, path string) permissions {
	var permissions permissions

	acls, err := fsclient.ListACLsWithGroupUsers(path)
	if err == nil {
		permissions.Readable, permissions.AccessLevelString = checkPermissions(acls, username)
	}
	return permissions
}

func checkPermissions(acls []*irodsfs_clienttype.IRODSAccess, username string) (bool, string) {
	highestAccessLevel := irodsfs_clienttype.IRODSAccessLevelNone
	for _, acl := range acls {
		if acl.UserName == username {
			switch acl.AccessLevel {
			case irodsfs_clienttype.IRODSAccessLevelOwner:
				highestAccessLevel = irodsfs_clienttype.IRODSAccessLevelOwner
			case irodsfs_clienttype.IRODSAccessLevelWrite:
				if highestAccessLevel == irodsfs_clienttype.IRODSAccessLevelNone || highestAccessLevel == irodsfs_clienttype.IRODSAccessLevelRead {
					highestAccessLevel = irodsfs_clienttype.IRODSAccessLevelWrite
				}
			case irodsfs_clienttype.IRODSAccessLevelRead:
				if highestAccessLevel == irodsfs_clienttype.IRODSAccessLevelNone {
					highestAccessLevel = irodsfs_clienttype.IRODSAccessLevelRead
				}
			}
		}
	}

	if highestAccessLevel != irodsfs_clienttype.IRODSAccessLevelNone {
		return true, getAccessLevelString(highestAccessLevel)
	}

	return false, ""
}

func getAccessLevelString(accessLevel irodsfs_clienttype.IRODSAccessLevelType) string {

	switch accessLevel {
	case irodsfs_clienttype.IRODSAccessLevelOwner:
		return "own"
	case irodsfs_clienttype.IRODSAccessLevelWrite:
		return "write"
	case irodsfs_clienttype.IRODSAccessLevelRead:
		return "read"
	}

	return "?"
}

func makeCheck(msg *nats.Msg) check {
	var check check
	var o map[string]interface{}
	if err := json.Unmarshal(msg.Data, &o); err != nil {
		slog.Error("json unmarshal error", "error", err)
	}
	check.path = o["path"].(string)
	check.username = o["user"].(string)
	check.permissions = make(chan permissions)
	return check
}

func worker(checks <-chan check) {
	for check := range checks {
		check.permissions <- checkPath(check.username, check.path)
	}
}
