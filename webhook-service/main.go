package main

import (
	"bytes"
	"container/heap"
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"log/slog"
	"mime/multipart"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"

	"github.com/nats-io/stan.go"
	"gitlab.com/cyverse/datawatch/common"
)

// message - data sent from monitor-service
type message struct {
	Details        string `json:"details"`
	ListenerID     string `json:"listenerID"`
	NotifyInterval int64  `json:"notifyInterval"`
	Message        string `json:"message"`
}

// notification - collection of events to send
type notification struct {
	Details        string
	Messages       []string
	ListenerID     string
	NotifyInterval int64
	Timestamp      int64
}

var notifications = make(map[string]*notification)
var pq = make(priorityQueue, 0)

var lock sync.RWMutex

func main() {
	heap.Init(&pq)

	monitorConnection := common.StanConnection("webhook", "monitor")
	defer monitorConnection.Close()

	sub, err := monitorConnection.Subscribe("datawatch.webhook", func(msg *stan.Msg) {
		// slog.Info("Received on msg", "subject", msg.Subject, "pid", os.Getpid(), "msgData", string(msg.Data))
		var m message
		json.Unmarshal(msg.Data, &m)
		lock.Lock()
		v, ok := notifications[m.ListenerID]
		if ok {
			v.Messages = append(v.Messages, m.Message)
		} else {
			if m.NotifyInterval < common.WebhookListenerType.MinNotifyInterval {
				m.NotifyInterval = common.WebhookListenerType.MinNotifyInterval
			}
			n := notification{m.Details, []string{m.Message}, m.ListenerID, m.NotifyInterval, time.Now().Unix() + int64(m.NotifyInterval)}
			notifications[n.ListenerID] = &n
			pq.Push(&n)
		}
		lock.Unlock()
	})
	if err != nil {
		log.Fatalf("Problem subscribing to datawatch.webhook: %s", err)
	}
	defer sub.Unsubscribe()

	go notify()

	slog.Info("webhook service waiting for messages, press ctrl+c to exit")
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	slog.Info("exiting webhook service")
}

func notify() {
	for {
		lock.Lock()
		now := time.Now().Unix()
		for pq.Len() > 0 && pq[0].Timestamp <= now {
			n := heap.Pop(&pq).(*notification)
			delete(notifications, n.ListenerID)
			details := struct {
				Authentication string   `json:"authentication"`
				Headers        []string `json:"headers"`
				Method         string   `json:"method"`
				Params         []string `json:"params"`
				URL            string   `json:"url"`
			}{}
			json.Unmarshal([]byte(n.Details), &details)
			if len(details.Params) > 0 {
				details.Method = "POST"
			}
			go webhook(details.URL, details.Method, details.Authentication, details.Headers, details.Params, n.Messages)
		}
		lock.Unlock()
		time.Sleep(1 * time.Minute)
	}
}

func webhook(url string, method string, authentication string, headers []string, params []string, messages []string) {
	if !strings.Contains(url, "://") {
		url = "http://" + url
	}
	var a []interface{}
	for _, m := range messages {
		var o map[string]interface{}
		json.Unmarshal([]byte(m), &o)
		a = append(a, o)
	}
	j, _ := json.MarshalIndent(a, "", "    ")
	client := &http.Client{}
	var req *http.Request
	var err error
	if params != nil {
		body := new(bytes.Buffer)
		writer := multipart.NewWriter(body)
		for _, param := range params {
			if i := strings.Index(param, "="); i != -1 {
				name := param[0:i]
				value := param[i+1:]
				if name == "key" {
					t := time.Now().UTC()
					ts := t.Format("2006-01-02-15-04-05.000")
					index := strings.LastIndex(value, ".")
					if index == -1 {
						value += ts
					} else {
						value = value[0:index] + ts + value[index:]
					}
				}
				if err := writer.WriteField(name, value); err != nil {
					writer.Close()
					slog.Error("fail to write multipart field", "error", err)
					return
				}
			}
		}
		if err := writer.WriteField("file", string(j)); err != nil {
			writer.Close()
			slog.Error("fail to write multipart field", "error", err)
			return
		}
		if err = writer.Close(); err != nil {
			slog.Error("fail to close multipart writer", "error", err)
			return
		}
		slog.Info("webhook notification", "method", "POST", "url", url, "body", string(j))
		req, err = http.NewRequest("POST", url, body)
		req.Header.Add("Content-Type", "multipart/form-data; boundary="+writer.Boundary())
	} else {
		switch method {
		case "POST":
			slog.Info("webhook notification", "method", method, "url", url, "body", string(j))
			req, err = http.NewRequest("POST", url, bytes.NewReader(j))
			req.Header.Add("Content-Type", "application/json; charset=UTF-8")
		case "PUT":
			slog.Info("webhook notification", "method", method, "url", url, "body", string(j))
			req, err = http.NewRequest("PUT", url, bytes.NewReader(j))
			req.Header.Add("Content-Type", "application/json; charset=UTF-8")
		default:
			slog.Info("webhook notification", "method", method, "url", url+"?events="+string(j))
			req, err = http.NewRequest("GET", url+"?events="+string(j), nil)
		}
	}
	if err != nil {
		slog.Error("fail to create new request", "error", err)
		return
	}
	if authentication != "" {
		encoded := base64.StdEncoding.EncodeToString([]byte(authentication))
		req.Header.Add("Authorization", "Basic "+encoded)
	}
	for _, h := range headers {
		i := strings.Index(h, ":")
		if i != -1 {
			req.Header.Add(h[0:i], h[i+1:])
		}
	}
	res, err := client.Do(req)
	if err != nil {
		slog.Error("fail to do request", "error", err)
		return
	}
	defer res.Body.Close()
	bytes, err := io.ReadAll(res.Body)
	if err != nil {
		slog.Error("fail to read response body", "error", err)
		return
	}
	slog.Info("webhook response", "resp_body", string(bytes), "status", res.StatusCode, "method", req.Method, "uri", req.RequestURI)
}
