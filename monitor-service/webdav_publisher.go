package main

import (
	"encoding/json"
	"log/slog"

	"github.com/nats-io/stan.go"
	"gitlab.com/cyverse/datawatch/common"
)

// webdavPublisher - publishes notifications to webdav-service
type webdavPublisher struct {
	connection stan.Conn
}

// close - clean up on program exit
func (p *webdavPublisher) close() {
	p.connection.Close()
}

// init - configure NATS connection to webdav-service
func (p *webdavPublisher) init() {
	p.connection = common.StanConnection("monitor", "webdav")
}

// publish - send notification to webdav-service
func (p *webdavPublisher) publish(listener *common.Listener, message string) {
	json, _ := json.Marshal(notification{listener.Details, listener.ListenerID, listener.NotifyInterval, message})
	slog.Info("publishing msg", "json", string(json))
	p.connection.Publish("datawatch.webdav", json)
}
