package main

import (
	"encoding/json"
	"log/slog"

	"github.com/nats-io/stan.go"
	"gitlab.com/cyverse/datawatch/common"
)

// webhookPublisher - publishes notifications to webhook-service
type webhookPublisher struct {
	connection stan.Conn
}

// close - clean up on program exit
func (p *webhookPublisher) close() {
	p.connection.Close()
}

// init - configure NATS connection to webhook-service
func (p *webhookPublisher) init() {
	p.connection = common.StanConnection("monitor", "webhook")
}

// publish - send notification to webhook-service
func (p *webhookPublisher) publish(listener *common.Listener, message string) {
	json, _ := json.Marshal(notification{listener.Details, listener.ListenerID, listener.NotifyInterval, message})
	slog.Info("publishing msg", "json", string(json))
	p.connection.Publish("datawatch.webhook", json)
}
