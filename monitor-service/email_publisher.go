package main

import (
	"encoding/json"
	"log/slog"

	"github.com/nats-io/stan.go"
	"gitlab.com/cyverse/datawatch/common"
)

// emailPublisher - publishes notifications to email-service
type emailPublisher struct {
	connection stan.Conn
}

// close - clean up on program exit
func (p *emailPublisher) close() {
	p.connection.Close()
}

// init - configure NATS connection to email-service
func (p *emailPublisher) init() {
	p.connection = common.StanConnection("monitor", "email")
}

// publish - send notification to email-service
func (p *emailPublisher) publish(listener *common.Listener, message string) {
	details := struct {
		Email string `json:"email"`
	}{}
	json.Unmarshal([]byte(listener.Details), &details)
	if details.Email == "" {
		user, err := common.LoadUser(listener.Username)
		if err == nil {
			details.Email = user.Email
			j, _ := json.Marshal(details)
			listener.Details = string(j)
		}
	}
	j, _ := json.Marshal(notification{listener.Details, listener.ListenerID, listener.NotifyInterval, message})
	slog.Info("publishing msg", "json", string(j))
	p.connection.Publish("datawatch.email", j)
}
