package main

import (
	"encoding/json"
	"log"
	"log/slog"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/streadway/amqp"
	"gitlab.com/cyverse/datawatch/common"
)

var pathConnection *nats.Conn
var permissionsConnection *nats.Conn

var publishers map[string]publisher

var state struct {
	NumAdd            int64                `json:"numAdd"`
	NumEventsHandled  int64                `json:"numEventsHandled"`
	NumEventsReceived int64                `json:"numEventsReceived"`
	NumMod            int64                `json:"numMod"`
	NumMv             int64                `json:"numMv"`
	NumRm             int64                `json:"numRm"`
	Latest            map[string]time.Time `json:"latest"`
}

func main() {
	state.Latest = make(map[string]time.Time)

	common.InitDB()

	rmqUsername := url.QueryEscape(os.Getenv("rmqUname"))
	rmqPassword := url.QueryEscape(os.Getenv("rmqPswrd"))
	rmqHost := os.Getenv("rmqHost")
	rmqPort := os.Getenv("rmqPort")
	rmqVhost := os.Getenv("rmqVhost")
	rmqQueue := os.Getenv("rmqQueue")

	rmqConn, err := amqp.Dial("amqp://" + rmqUsername + ":" + rmqPassword + "@" + rmqHost + ":" + rmqPort + "/" + rmqVhost)
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %s", err)
	}
	defer rmqConn.Close()
	rmqChan, err := rmqConn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %s", err)
	}
	defer rmqChan.Close()

	apiConnection := common.NatsConnection()
	defer apiConnection.Close()
	pathConnection = common.NatsConnection()
	defer pathConnection.Close()
	permissionsConnection = common.NatsConnection()
	defer permissionsConnection.Close()

	publishers = make(map[string]publisher)
	publishers["email"] = &emailPublisher{}
	publishers["webdav"] = &webdavPublisher{}
	publishers["webhook"] = &webhookPublisher{}
	for _, v := range publishers {
		v.init()
		defer v.close()
	}

	sub, err := apiConnection.Subscribe("datawatch.api", func(msg *nats.Msg) {
		if string(msg.Data) == "state" {
			json, _ := json.Marshal(state)
			// slog.Info("Sending " + string(json))
			msg.Respond(json)
			return
		}
		var o map[string]interface{}
		if err := json.Unmarshal(msg.Data, &o); err != nil {
			slog.Error("json unmarshal error", "error", err)
		}
		if id, ok := o["trigger"]; ok {
			l, err := common.LoadListener(id.(string), o["username"].(string))
			if err != nil {
				slog.Error("fail to load listener from db", "error", err)
				return
			}
			if l != nil {
				publishers[l.ListenerTypeID].publish(l, o["data"].(string))
			}
		}
	})
	if err != nil {
		log.Fatalf("Problem subscribing to datawatch.api: %s", err)
	}
	defer sub.Unsubscribe()

	go monitor(rmqChan, rmqQueue)

	slog.Info("monitor-service waiting for messages, press ctrl+c to exit")
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	slog.Info("exiting monitor service")
}

func getPath(id string) string {
	j, _ := json.Marshal(map[string]interface{}{"id": id})
	// slog.Info("monitor-service sending to path-service: " + string(j))
	response, err := pathConnection.Request("datawatch.paths", j, 5*time.Second)
	if err != nil {
		if pathConnection.LastError() != nil {
			slog.Info("getPath last error", "error", pathConnection.LastError())
		}
		slog.Error("getPath request error", "error", err)
		return ""
	}
	// slog.Info("monitor-service received from path-service", "resp", string(response.Data))
	var path string
	json.Unmarshal(response.Data, &path)
	return path
}

func handleEvent(event, path string) error {
	// slog.Info(path)
	listeners, err := common.GetListenersOnPath(path)
	if err != nil {
		return err
	}
	for _, listener := range listeners {
		if !listener.EventMatches(event) {
			continue
		}
		if event == "removed" || userCanReadPath(path, listener.Username) { // can't check perms on file that's no longer there
			j, _ := json.Marshal(map[string]string{"event": event, "path": path})
			publishers[listener.ListenerTypeID].publish(listener, string(j))
			switch event {
			case "created":
				state.NumAdd++
			case "modified":
				state.NumMod++
			case "removed":
				state.NumRm++
			}
			state.NumEventsHandled++
			state.Latest[listener.Username] = time.Now()
		} else {
			slog.Info("user has no permission to read path", "username", listener.Username, "path", path)
		}
	}
	return nil
}

func handleMove(event, oldPath, newPath string) error {
	// slog.Info(path)
	listeners, err := common.GetListenersOnPath(oldPath)
	if err != nil {
		return err
	}
	for _, listener := range listeners {
		if !listener.EventMatches(event) {
			continue
		}
		if userCanReadPath(newPath, listener.Username) {
			j, _ := json.Marshal(map[string]string{"event": event, "old_path": oldPath, "new_path": newPath})
			publishers[listener.ListenerTypeID].publish(listener, string(j))
			state.NumMv++
			state.NumEventsHandled++
			state.Latest[listener.Username] = time.Now()
		} else {
			slog.Info("user has no permission to read path", "username", listener.Username, "path", newPath)
		}
	}
	return nil
}

func monitor(rmqChan *amqp.Channel, rmqQueue string) {
	for {
		msgs, err := rmqChan.Consume(
			rmqQueue, // queue
			"",       // consumer
			true,     // autoAck
			false,    // exclusive
			false,    // noLocal
			false,    // noWait
			nil,      // args
		)
		if err != nil {
			log.Fatalf("Failed to register an RMQ consumer: %s", err)
		}
		for msg := range msgs {
			state.NumEventsReceived++
			if msg.RoutingKey != "data-object.add" && msg.RoutingKey != "data-object.mod" && msg.RoutingKey != "data-object.mv" && msg.RoutingKey != "data-object.rm" {
				continue
			}
			body := make(map[string]interface{})
			if err := json.Unmarshal(msg.Body, &body); err != nil {
				slog.Error("json unmarshal error", "error", err)
				continue
			}
			// slog.Info("amqp msg", "body", body)

			// slog.Info(msg.RoutingKey)
			t := 0
			for {
				var err error
				switch msg.RoutingKey {
				case "data-object.add":
					err = handleEvent("created", body["path"].(string))
				case "data-object.mod":
					path := getPath(body["entity"].(string))
					if path != "" {
						err = handleEvent("modified", path)
					}
				case "data-object.mv":
					err = handleMove("moved", body["old-path"].(string), body["new-path"].(string))
				case "data-object.rm":
					err = handleEvent("removed", body["path"].(string))
				}
				if err == nil {
					break
				}
				// hopefully err was caused by the backup, keep trying every 30 seconds for up to 5 minutes
				if t >= 300 {
					break
				}
				t += 30
				time.Sleep(30 * time.Second)
			}
		}
		slog.Info("stopped consuming events")
	}
}

func userCanReadPath(path string, username string) bool {
	j, _ := json.Marshal(map[string]interface{}{"path": path, "user": username})
	slog.Info("monitor-service sending to perm-service", "json", string(j))
	response, err := permissionsConnection.Request("datawatch.permissions", j, 2*time.Second)
	if err != nil {
		if permissionsConnection.LastError() != nil {
			slog.Error("userCanReadPath last error", "error", permissionsConnection.LastError())
		}
		slog.Error("userCanReadPath request error", "error", err)
		return false
	}
	r := make(map[string]interface{})
	slog.Info("monitor-service received from perm-service", "resp", string(response.Data))
	json.Unmarshal(response.Data, &r)
	v, ok := r["readable"]
	return ok && v.(bool)
}
