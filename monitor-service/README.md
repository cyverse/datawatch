# DataWatch Monitor Service
- events are received via RabbitMQ
- events are matched against the listeners in the database, non-matching events are ignored
- matched events are sent to perm-service to check permissions for the listener's user
- events for which the user doesn't have permission for the paths are discarded
- otherwise events are sent to one of the notification services depending on listener configuration

## Configuration
monitor-service is configured via the following environment variables:
- rmqUsername
- rmqPassword
- rmqHost
- rmqPort
- rmqVhost
- rmqQueue
The template file config.env can be edited. It is used by docker-compose. There's also a config.yml that can be edited and used for kubernetes.
