package main

import "gitlab.com/cyverse/datawatch/common"

// notification - struct for sending notifications to services
type notification struct {
	Details        string `json:"details"`
	ListenerID     string `json:"listenerID"`
	NotifyInterval int64  `json:"notifyInterval,omitempty"`
	Message        string `json:"message"`
}

// publisher - interface for publishing notifications to services
type publisher interface {
	close()
	init()
	publish(listener *common.Listener, message string)
}
